package swo

import "golang.org/x/exp/constraints"

func Min[O constraints.Ordered](a, b O) O {
	if a < b {
		return a
	}
	return b
}
