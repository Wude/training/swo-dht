package swo

import (
	"math"
	"sync"
	"sync/atomic"
)

// A read-write-mutex which groups lockers by keys.
type KeyedRWMutex struct {
	pool    sync.Pool                  // An object pool for lockers by keys.
	mu      sync.RWMutex               // A locker for handling the map.
	lockers map[any]*KeyedRWMutexEntry // A map of lockers by their keys.
}

// A keyed read-write-mutex.
type KeyedRWMutexEntry struct {
	k     *KeyedRWMutex
	key   any
	count int32 // Counter for potential lockers; negative -> entry marked for deletion
	mu    sync.RWMutex
}

func (k *KeyedRWMutex) newRWMutexEntry() any {
	return &KeyedRWMutexEntry{k: k}
}

// Initialize the given keyed read-write-mutex.
func (k *KeyedRWMutex) Init() {
	k.pool.New = k.newRWMutexEntry
	k.lockers = make(map[any]*KeyedRWMutexEntry)
}

// Load or store the keyed [Locker].
func (k *KeyedRWMutex) loadOrStore(key any) *KeyedRWMutexEntry {
	k.mu.RLock()
	rw, ok := k.lockers[key]
	k.mu.RUnlock()
	if !ok {
		k.mu.Lock()
		rw, ok = k.lockers[key]
		if !ok {
			rw = k.pool.Get().(*KeyedRWMutexEntry)
			rw.key = key
			k.lockers[key] = rw
		}
		k.mu.Unlock()
	}
	return rw
}

// Load a keyed [Locker].
func (k *KeyedRWMutex) load(key any) *KeyedRWMutexEntry {
	k.mu.RLock()
	rw, ok := k.lockers[key]
	k.mu.RUnlock()
	if !ok {
		panic("keyed read write mutex already unlocked")
	}
	return rw
}

// Delete the keyed [Locker].
func (rw *KeyedRWMutexEntry) delete() {
	rw.k.mu.Lock()
	delete(rw.k.lockers, rw.key)
	rw.k.pool.Put(rw)
	rw.k.mu.Unlock()
}

// Load or store the keyed [Locker] and block it.
func (k *KeyedRWMutex) loadOrStoreAndBlock(key any) (rw *KeyedRWMutexEntry) {
	for {
		rw = k.loadOrStore(key)
		count := atomic.LoadInt32(&rw.count)
		if count >= 0 && atomic.CompareAndSwapInt32(&rw.count, count, count+1) {
			break
		}
	}
	return
}

// Delete the keyed [Locker] unless it is blocked.
func (rw *KeyedRWMutexEntry) deleteIfNotBlocked() {
	// Delete the entry, if there are no other blockers
	if atomic.LoadInt32(&rw.count) == 1 && atomic.CompareAndSwapInt32(&rw.count, 1, 1+math.MinInt32) {
		rw.delete()
		// Reset the block that prevented others from using that entry
		if !atomic.CompareAndSwapInt32(&rw.count, 1+math.MinInt32, 1) {
			panic("keyed lock entry in deletion was used")
		}
	}
}

// Unblock the keyed [Locker].
func (rw *KeyedRWMutexEntry) unblock() {
	atomic.AddInt32(&rw.count, -1)
}

// Lock a key for reading.
func (k *KeyedRWMutex) RLock(key any) *KeyedRWMutexEntry {
	rw := k.loadOrStoreAndBlock(key)
	rw.RLock()
	return rw
}

// Lock a keyed [Locker] for reading.
func (rw *KeyedRWMutexEntry) RLock() {
	rw.mu.RLock()
}

// Unlock a key for reading.
func (k *KeyedRWMutex) RUnlock(key any) {
	k.load(key).RUnlock()
}

// Unlock a keyed [Locker] which was locked for reading.
func (rw *KeyedRWMutexEntry) RUnlock() {
	rw.deleteIfNotBlocked()
	rw.mu.RUnlock()
	rw.unblock()
}

// Lock a key for writing.
func (k *KeyedRWMutex) Lock(key any) *KeyedRWMutexEntry {
	rw := k.loadOrStoreAndBlock(key)
	rw.Lock()
	return rw
}

// Lock a keyed [Locker] for writing.
func (rw *KeyedRWMutexEntry) Lock() {
	rw.mu.Lock()
}

// Unlock a key for writing.
func (k *KeyedRWMutex) Unlock(key any) {
	k.load(key).Unlock()
}

// Unlock a keyed [Locker] which was locked for writing.
func (rw *KeyedRWMutexEntry) Unlock() {
	rw.deleteIfNotBlocked()
	rw.mu.Unlock()
	rw.unblock()
}
