package swo

import (
	"bytes"
	"sync"
	"text/template"
)

var templatePool *sync.Pool

func init() {
	templatePool = &sync.Pool{}
	templatePool.New = func() any { return template.New("Sprintf") }
}

// An implementation of `Sprintf` with template usage (see package "text/template").
func Sprintf(tpl string, data any) (str string) {
	t := templatePool.Get().(*template.Template)
	defer templatePool.Put(t)

	var err error
	if t, err = t.Parse(tpl); err != nil {
		return
	}
	buf := &bytes.Buffer{}
	if err = t.Execute(buf, data); err != nil {
		return
	}
	str = buf.String()
	return
}
