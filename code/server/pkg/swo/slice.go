package swo

import "golang.org/x/exp/constraints"

// Reverse the slice items.
func Reverse[T any](slice []T) {
	l := len(slice)
	if l >= 2 {
		u := l - 1
		var temp T
		for i := l / 2; i >= 0; i-- {
			temp = slice[i]
			slice[i] = slice[u-i]
			slice[u-i] = temp
		}
	}
}

// Copy and reverse a slice.
func CopyAndReverse[T any](dst []T, src []T) {
	copy(dst[:], src[:])
	Reverse(dst[:])
}

// Reversed copy of the slice.
func ReversedCopy[T any](slice []T) []T {
	l := len(slice)
	result := make([]T, l)
	copy(result[:], slice[:])
	Reverse(result[:])
	return result
}

// Does the slice contain the given element? WARNING: Worst run time is O(n)!
func Contains[C comparable](slice []C, elem C) bool {
	for _, v := range slice {
		if v == elem {
			return true
		}
	}
	return false
}

// At which index does the slice contain the given element? WARNING: The slice has to be sorted already!
func BinarySearch[C constraints.Ordered](slice []C, elem C) int {
	lowerIndex := 0
	upperIndex := len(slice) - 1

	for lowerIndex <= upperIndex {
		middleIndex := lowerIndex + (upperIndex - lowerIndex>>1)
		if slice[middleIndex] < elem {
			lowerIndex = middleIndex + 1
		} else if slice[middleIndex] > elem {
			upperIndex = middleIndex - 1
		} else {
			return middleIndex
		}
	}
	return ^lowerIndex
}

// Does the slice contain the given element? WARNING: The slice has to be sorted already!
func SortedContains[C constraints.Ordered](slice []C, elem C) bool {
	return BinarySearch(slice, elem) >= 0
}
