//go:build windows

package swo

import "syscall"

// See: https://docs.microsoft.com/en-us/windows/console/getconsolemode#parameters
const (
	enableVirtualTerminalProcessingMode uint32 = 0x4
)

var (
	kernel32           *syscall.LazyDLL
	procSetConsoleMode *syscall.LazyProc
)

func init() {
	if kernel32 != nil {
		return
	}

	// See: https://docs.microsoft.com/en-us/windows/console/setconsolemode
	kernel32 = syscall.NewLazyDLL("kernel32.dll")
	procSetConsoleMode = kernel32.NewProc("SetConsoleMode")
}

func enableVirtualTerminalProcessing(stream syscall.Handle, enable bool) error {
	var mode uint32
	err := syscall.GetConsoleMode(stream, &mode)
	if err != nil {
		return err
	}

	if enable {
		mode |= enableVirtualTerminalProcessingMode
	} else {
		mode &^= enableVirtualTerminalProcessingMode
	}

	ret, _, err := procSetConsoleMode.Call(uintptr(stream), uintptr(mode))
	if ret == 0 {
		return err
	}

	return nil
}

func TryEnableColorMode() (err error) {
	err = enableVirtualTerminalProcessing(syscall.Stdin, true)
	if err == nil {
		err = enableVirtualTerminalProcessing(syscall.Stdout, true)
	}
	return
}

func TryDisableColorMode() (err error) {
	err = enableVirtualTerminalProcessing(syscall.Stdin, false)
	if err == nil {
		err = enableVirtualTerminalProcessing(syscall.Stdout, false)
	}
	return
}
