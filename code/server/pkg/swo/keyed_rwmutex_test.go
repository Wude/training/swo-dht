package swo

import (
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type krwFunc = func(t *testing.T, start time.Time, index int, wg *sync.WaitGroup, krw *KeyedRWMutex, resMu *sync.Mutex, res *[]rune, key string)

func kwFunc(t *testing.T, start time.Time, index int, wg *sync.WaitGroup, krw *KeyedRWMutex, resMu *sync.Mutex, res *[]rune, key string) {
	wg.Add(1)
	krw.Lock(key)
	time.Sleep(500 * time.Millisecond)
	now := time.Now()
	diff := now.Sub(start)
	t.Logf("writer #%d: %s\n", index, diff)
	resMu.Lock()
	*res = append(*res, 'w')
	resMu.Unlock()
	krw.Unlock(key)
	wg.Done()
}

func krFunc(t *testing.T, start time.Time, index int, wg *sync.WaitGroup, krw *KeyedRWMutex, resMu *sync.Mutex, res *[]rune, key string) {
	wg.Add(1)
	krw.RLock(key)
	time.Sleep(100 * time.Millisecond)
	now := time.Now()
	diff := now.Sub(start)
	t.Logf("reader #%d: %s\n", index, diff)
	resMu.Lock()
	*res = append(*res, 'r')
	resMu.Unlock()
	krw.RUnlock(key)
	wg.Done()
}

func goKRWFuncs(t *testing.T, krwFunc1 krwFunc, krwFunc2 krwFunc) (results []rune) {
	var resultMutex sync.Mutex

	wg := sync.WaitGroup{}

	var krw KeyedRWMutex
	krw.Init()
	const key = "key"
	start := time.Now()

	var fIds = []int{1, 1, 2, 2, 2, 2, 2, 1, 1, 1} // Test order for r/w-Funcs
	var fCnt1 = 0
	var fCnt2 = 0

	for i := 0; i < len(fIds); i++ {
		if fIds[i] == 1 {
			go krwFunc1(t, start, fCnt1, &wg, &krw, &resultMutex, &results, key)
			fCnt1++
		} else {
			go krwFunc2(t, start, fCnt2, &wg, &krw, &resultMutex, &results, key)
			fCnt2++
		}
		time.Sleep(1 * time.Millisecond) // Use a small time gap between spawns to ensure test order
	}

	t.Logf("all functions spawned.\n")
	wg.Wait()
	t.Logf("all functions ended.\n")
	return
}

// Output should be like the following:
// ```
// reader #0: 101.2371ms
// reader #1: 116.4527ms
// all functions spawned.
// writer #0: 629.2037ms
// reader #4: 738.3603ms
// reader #2: 738.3603ms
// reader #3: 738.3603ms
// writer #1: 1.2386594s
// writer #2: 1.753357s
// writer #3: 2.2673009s
// writer #4: 2.7820433s
// all functions ended.
// ```
func TestRWKeyedMutexRW(t *testing.T) {
	// Start order: 2 readers, 5 writers, 3 readers
	// Result: just one writer may go in between the readers
	results := goKRWFuncs(t, krFunc, kwFunc)
	assert.Equal(t, 'r', results[0]) // reader
	assert.Equal(t, 'r', results[1]) // reader
	assert.Equal(t, 'w', results[2]) // writer
	assert.Equal(t, 'r', results[3]) // reader
	assert.Equal(t, 'r', results[4]) // reader
	assert.Equal(t, 'r', results[5]) // reader
	assert.Equal(t, 'w', results[6]) // writer
	assert.Equal(t, 'w', results[7]) // writer
	assert.Equal(t, 'w', results[8]) // writer
	assert.Equal(t, 'w', results[9]) // writer
}

// Output should be like the following:
// ```
// all functions spawned.
// writer #0: 515.4316ms
// reader #0: 623.8886ms
// reader #4: 623.8886ms
// reader #1: 623.8886ms
// reader #2: 623.8886ms
// reader #3: 623.8886ms
// writer #1: 1.136473s
// writer #2: 1.6578395s
// writer #3: 2.1630119s
// writer #4: 2.6745873s
// all functions ended.
// ```
func TestRWKeyedMutexWR(t *testing.T) {
	// Start order: 2 writers, 5 readers, 3 writers
	// Result: all readers run after the first writer
	results := goKRWFuncs(t, kwFunc, krFunc)
	assert.Equal(t, 'w', results[0]) // writer
	assert.Equal(t, 'r', results[1]) // reader
	assert.Equal(t, 'r', results[2]) // reader
	assert.Equal(t, 'r', results[3]) // reader
	assert.Equal(t, 'r', results[4]) // reader
	assert.Equal(t, 'r', results[5]) // reader
	assert.Equal(t, 'w', results[6]) // writer
	assert.Equal(t, 'w', results[7]) // writer
	assert.Equal(t, 'w', results[8]) // writer
	assert.Equal(t, 'w', results[9]) // writer
}
