package swo

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	log "github.com/sirupsen/logrus"
)

const (
	periodicTaskDescription string = "periodic"
	shutdownTaskDescription string = "shutdown"
)

var (
	shutdownWaitDuration = 10 * time.Second
)

type (
	Action func() error
)

type taskManager struct {
	mu                     sync.RWMutex
	sig                    chan os.Signal
	shutdownBeginCtx       context.Context
	shutdownBeginCtxCancel context.CancelFunc
	shutdownDoneCtx        context.Context
	shutdownDoneCtxCancel  context.CancelFunc
	taskWaitGroup          sync.WaitGroup
	errors                 []error
	runAtShutdown          []Action
}

var theTaskManager *taskManager

func init() {
	theTaskManager = &taskManager{}
	theTaskManager.shutdownBeginCtx, theTaskManager.shutdownBeginCtxCancel = context.WithCancel(context.Background())
	theTaskManager.shutdownDoneCtx, theTaskManager.shutdownDoneCtxCancel = context.WithCancel(context.Background())

	go handleSignals()
}

func handleSignals() {
	theTaskManager.sig = make(chan os.Signal, 1)
	signal.Notify(theTaskManager.sig, syscall.SIGINT, syscall.SIGTERM)
	defer signal.Stop(theTaskManager.sig)

	select {
	case <-theTaskManager.sig:
	case <-theTaskManager.shutdownBeginCtx.Done():
	}
	tryGracefulShutdown()
}

func tryGracefulShutdown() {
	theTaskManager.shutdownBeginCtxCancel()

	for _, action := range theTaskManager.runAtShutdown {
		theTaskManager.taskWaitGroup.Add(1)
		go runAction(shutdownTaskDescription, action)
	}

	go func() {
		// Wait for normal shutdown
		theTaskManager.taskWaitGroup.Wait()
		theTaskManager.mu.Lock()
		theTaskManager.shutdownDoneCtxCancel()
		theTaskManager.mu.Unlock()
	}()
	// Force shutdown after timeout
	time.Sleep(shutdownWaitDuration)
	theTaskManager.mu.Lock()
	theTaskManager.shutdownDoneCtxCancel()
	theTaskManager.mu.Unlock()
}

func runAction(taskDescription string, action Action) {
	defer theTaskManager.taskWaitGroup.Done()

	defer func() {
		if err := recover(); err != nil {
			msg := fmt.Errorf("panic in %s task: %v", taskDescription, err)
			log.Error(msg)
			theTaskManager.mu.Lock()
			theTaskManager.errors = append(theTaskManager.errors, msg)
			theTaskManager.mu.Unlock()
		}
	}()

	if err := action(); err != nil {
		theTaskManager.mu.Lock()
		theTaskManager.errors = append(theTaskManager.errors, err)
		theTaskManager.mu.Unlock()
	}
}

// Start a goroutine that executes the given action once.
func Run(action func(context.Context)) (ctx context.Context, cancel context.CancelFunc) {
	ctx, cancel = context.WithCancel(context.Background())
	go func() {
		action(ctx)
		cancel()
	}()
	return
}

// Start a goroutine that executes the given action periodically.
func RunPeriodically(interval time.Duration, action Action) (ctx context.Context, cancel context.CancelFunc) {
	ctx, cancel = context.WithCancel(context.Background())
	ticker := time.NewTicker(interval)
	theTaskManager.taskWaitGroup.Add(1)
	go runAction(periodicTaskDescription, func() (err error) {
		for {
			select {
			case <-ticker.C:
				err = action()
				if err != nil {
					ticker.Stop()
					return
				}
			case <-theTaskManager.shutdownBeginCtx.Done():
				cancel()
			case <-ctx.Done():
				err = ctx.Err()
				ticker.Stop()
				return
			}
		}
	})
	return
}

// Run the given action at shutdown.
func RunAtShutdown(action Action) {
	theTaskManager.mu.Lock()
	theTaskManager.runAtShutdown = append(theTaskManager.runAtShutdown, action)
	theTaskManager.mu.Unlock()
}

// Wait for program termination signal.
func WaitForTermination() {
	<-theTaskManager.shutdownDoneCtx.Done()
}
