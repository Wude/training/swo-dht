package swo

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestContains(t *testing.T) {
	slice := []string{"P1", "P1", "P1", "P1", "P1", "S15B", "S5B", "S5E"}
	assert.True(t, Contains(slice[5:7], "S15B"))
	assert.True(t, Contains(slice[5:7], "S5B"))
	assert.False(t, Contains(slice[5:7], "S5E"))
}

func TestBinarySearch(t *testing.T) {
	slice := []string{"P1", "P1", "P1", "P1", "P1", "S15B", "S5B", "S5E"}

	assert.Equal(t, 0, BinarySearch(slice[4:8], slice[4]))
	assert.Equal(t, 1, BinarySearch(slice[4:8], slice[5]))
	assert.Equal(t, 2, BinarySearch(slice[4:8], slice[6]))
	assert.Equal(t, 3, BinarySearch(slice[4:8], slice[7]))
	assert.Equal(t, ^1, BinarySearch(slice[4:8], "Q"))
	assert.Equal(t, ^2, BinarySearch(slice[4:8], "S16"))
}
