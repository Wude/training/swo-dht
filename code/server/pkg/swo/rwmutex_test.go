package swo

import (
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type rwFunc = func(t *testing.T, start time.Time, index int, wg *sync.WaitGroup, rw *sync.RWMutex, resMu *sync.Mutex, res *[]rune)

func wFunc(t *testing.T, start time.Time, index int, wg *sync.WaitGroup, rw *sync.RWMutex, resMu *sync.Mutex, res *[]rune) {
	wg.Add(1)
	rw.Lock()
	time.Sleep(500 * time.Millisecond)
	now := time.Now()
	diff := now.Sub(start)
	t.Logf("writer #%d: %s\n", index, diff)
	resMu.Lock()
	*res = append(*res, 'w')
	resMu.Unlock()
	rw.Unlock()
	wg.Done()
}

func rFunc(t *testing.T, start time.Time, index int, wg *sync.WaitGroup, rw *sync.RWMutex, resMu *sync.Mutex, res *[]rune) {
	wg.Add(1)
	rw.RLock()
	time.Sleep(100 * time.Millisecond)
	now := time.Now()
	diff := now.Sub(start)
	t.Logf("reader #%d: %s\n", index, diff)
	resMu.Lock()
	*res = append(*res, 'r')
	resMu.Unlock()
	rw.RUnlock()
	wg.Done()
}

func goRWFuncs(t *testing.T, rwFunc1 rwFunc, rwFunc2 rwFunc) (results []rune) {
	var resultMutex sync.Mutex

	wg := sync.WaitGroup{}

	var rw sync.RWMutex
	start := time.Now()

	var fIds = []int{1, 1, 2, 2, 2, 2, 2, 1, 1, 1} // Test order for r/w-Funcs
	var fCnt1 = 0
	var fCnt2 = 0

	for i := 0; i < len(fIds); i++ {
		if fIds[i] == 1 {
			go rwFunc1(t, start, fCnt1, &wg, &rw, &resultMutex, &results)
			fCnt1++
		} else {
			go rwFunc2(t, start, fCnt2, &wg, &rw, &resultMutex, &results)
			fCnt2++
		}
		time.Sleep(1 * time.Millisecond) // Use a small time gap between spawns to ensure test order
	}

	t.Logf("all functions spawned.\n")
	wg.Wait()
	t.Logf("all functions ended.\n")
	return
}

// Output should be like the following:
// ```
// reader #0: 109.3324ms
// reader #1: 124.5679ms
// all functions spawned.
// writer #0: 639.4134ms
// reader #2: 748.9815ms
// reader #3: 748.9815ms
// reader #4: 748.9815ms
// writer #1: 1.2511384s
// writer #2: 1.7630547s
// writer #3: 2.2656381s
// writer #4: 2.7787347s
// all functions ended.
// ```
func TestRWMutexRW(t *testing.T) {
	// Start order: 2 readers, 5 writers, 3 readers
	// Result: just one writer may go in between the readers
	results := goRWFuncs(t, rFunc, wFunc)
	assert.Equal(t, 'r', results[0]) // reader
	assert.Equal(t, 'r', results[1]) // reader
	assert.Equal(t, 'w', results[2]) // writer
	assert.Equal(t, 'r', results[3]) // reader
	assert.Equal(t, 'r', results[4]) // reader
	assert.Equal(t, 'r', results[5]) // reader
	assert.Equal(t, 'w', results[6]) // writer
	assert.Equal(t, 'w', results[7]) // writer
	assert.Equal(t, 'w', results[8]) // writer
	assert.Equal(t, 'w', results[9]) // writer
}

// Output should be like the following:
// ```
// all functions spawned.
// writer #0: 509.7256ms
// reader #3: 619.6235ms
// reader #0: 619.6235ms
// reader #2: 619.6235ms
// reader #1: 619.6235ms
// reader #4: 619.6235ms
// writer #1: 1.1334973s
// writer #2: 1.6367724s
// writer #3: 2.1507013s
// writer #4: 2.6654484s
// all functions ended.
// ```
func TestRWMutexWR(t *testing.T) {
	// Start order: 2 writers, 5 readers, 3 writers
	// Result: all readers run after the first writer
	results := goRWFuncs(t, wFunc, rFunc)
	assert.Equal(t, 'w', results[0]) // writer
	assert.Equal(t, 'r', results[1]) // reader
	assert.Equal(t, 'r', results[2]) // reader
	assert.Equal(t, 'r', results[3]) // reader
	assert.Equal(t, 'r', results[4]) // reader
	assert.Equal(t, 'r', results[5]) // reader
	assert.Equal(t, 'w', results[6]) // writer
	assert.Equal(t, 'w', results[7]) // writer
	assert.Equal(t, 'w', results[8]) // writer
	assert.Equal(t, 'w', results[9]) // writer
}
