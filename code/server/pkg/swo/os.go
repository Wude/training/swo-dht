package swo

import (
	"os"
	"reflect"
	"strconv"

	"golang.org/x/exp/constraints"
)

// Get an environment variable or else a default value.
func GetEnvOrDefault(key string, defaultValue string) string {
	data, success := os.LookupEnv(key)
	if success {
		return data
	}
	return defaultValue
}

// Get an environment variable converted to an signed integer number or else a default value.
func GetEnvOrDefaultInt[I constraints.Signed](key string, defaultValue I) I {
	data, success := os.LookupEnv(key)
	if success {
		num, err := strconv.ParseInt(data, 10, reflect.TypeOf(defaultValue).Bits())
		if err == nil {
			return I(num)
		}
	}
	return defaultValue
}

// Get an environment variable converted to an unsigned integer number or else a default value.
func GetEnvOrDefaultUint[I constraints.Unsigned](key string, defaultValue I) I {
	data, success := os.LookupEnv(key)
	if success {
		num, err := strconv.ParseUint(data, 10, reflect.TypeOf(defaultValue).Bits())
		if err == nil {
			return I(num)
		}
	}
	return defaultValue
}
