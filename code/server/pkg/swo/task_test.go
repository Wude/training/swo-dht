package swo

import (
	"context"
	"sync"
	"syscall"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestContext(t *testing.T) {
	var (
		workTimeCost  = 6 * time.Second
		cancelTimeout = 5 * time.Second
	)

	ctx, cancel := context.WithCancel(context.Background())

	var (
		data   int
		readCh = make(chan struct{})
	)
	go func() {
		defer close(readCh)
		log.Infoln("blocked to read data")
		// fake long i/o operations
		time.Sleep(workTimeCost)
		data = 10
		log.Infof("done read data (%d)\n", data)
	}()

	// fake cancel is called from the other routine (it's actually not caused by timeout)
	time.AfterFunc(cancelTimeout, cancel)

	select {
	case <-ctx.Done():
		log.Infoln("cancelled")
		return
	case <-readCh:
		log.Infoln("\"readCh\" closed")
		break
	}
}

func TestTasks(t *testing.T) {
	shutdownWaitDuration = 1 * time.Second

	var resultLock sync.Mutex
	results := []string{}

	_, _ = RunPeriodically(100*time.Millisecond, func() error {
		log.Infoln("RunPeriodically(100*time.Millisecond, ...)")
		resultLock.Lock()
		results = append(results, "P1")
		resultLock.Unlock()
		return nil
	})
	RunAtShutdown(func() error {
		log.Infoln("RunAtShutdown() { time.Sleep(500 * time.Millisecond) } // start")
		resultLock.Lock()
		results = append(results, "S5B")
		resultLock.Unlock()
		time.Sleep(500 * time.Millisecond)
		log.Infoln("RunAtShutdown() { time.Sleep(500 * time.Millisecond) } // end")
		resultLock.Lock()
		results = append(results, "S5E")
		resultLock.Unlock()
		return nil
	})
	RunAtShutdown(func() error {
		log.Infoln("RunAtShutdown() { time.Sleep(5000 * time.Millisecond) } // start")
		resultLock.Lock()
		results = append(results, "S15B")
		resultLock.Unlock()
		time.Sleep(5000 * time.Millisecond)
		log.Infoln("RunAtShutdown() { time.Sleep(5000 * time.Millisecond) } // end")
		resultLock.Lock()
		results = append(results, "S15E")
		resultLock.Unlock()
		return nil
	})

	go func() {
		// Imitate process termination
		time.Sleep(550 * time.Millisecond)
		theTaskManager.sig <- syscall.SIGINT
	}()

	WaitForTermination()

	// The periodic task should have been executed 5 times yielding 5 result entries
	// The first shutdown task should have been executed once yielding 2 result entries
	// The second shutdown task should have been started once but not ended yielding 1 result entry
	// Therefore we expect exactly 8 result entries and no test timeout
	assert.Equal(t, 8, len(results))
	assert.Equal(t, "P1", results[0])
	assert.Equal(t, "P1", results[1])
	assert.Equal(t, "P1", results[2])
	assert.Equal(t, "P1", results[3])
	assert.Equal(t, "P1", results[4])
	assert.True(t, Contains(results[5:7], "S15B"))
	assert.True(t, Contains(results[5:7], "S5B"))
	assert.Equal(t, "S5E", results[7])
}
