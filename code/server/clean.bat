@echo off
@REM Clean script for Windows in case no GNU make is available.

setlocal
set "SCRIPT_PATH=%~dp0"
set "SCRIPT_PATH=%SCRIPT_PATH:~0,-1%"
cd /d "%SCRIPT_PATH%"

call :clean chord
call :clean koorde

endlocal

exit /B %ERRORLEVEL%

:clean

set "PKG_NAME=%~1"

set "PROJECT=dht-%PKG_NAME%"

del /f /s /q bin\%PROJECT%.exe

exit /B 0
