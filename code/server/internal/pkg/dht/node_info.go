package dht

import (
	"bytes"
	"errors"
	"fmt"

	"gitlab.com/wude/training/swo-dht/pkg/swo"
)

type NodeInfo struct {
	id  Id     // The id of the node
	url string // The url of the node; should be "domain:port"
}

type nameFormattingInfo struct {
	Port int32
}

const (
	defaultDomain       = "localhost"
	defaultPort   int32 = 8000
)

func (n *NodeInfo) Init(url string) {
	n.id = MakeId(url)
	n.url = url
}

func MakeNodeInfo(idBytes []byte, url string) (n NodeInfo, err error) {
	n.Init(url)
	if !bytes.Equal(n.id.ToByteSlice(), idBytes) {
		err = errors.New("given id doesn't match computed id")
	}
	return
}

func (n *NodeInfo) InitByEnvAndGetPort(domainKey string, portKey string) (port int32) {
	port = swo.GetEnvOrDefaultInt(portKey, defaultPort)
	domain := swo.Sprintf(swo.GetEnvOrDefault(domainKey, defaultDomain), nameFormattingInfo{port})
	n.Init(fmt.Sprintf("%s:%d", domain, port))
	return
}

// Get the id of this node.
func (n *NodeInfo) Id() Id { return n.id }

// Get the url of this node.
func (n *NodeInfo) Url() string { return n.url }
