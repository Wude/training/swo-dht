package dht

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	id128 = maxId.AddPow2(128)
	id160 = maxId.AddPow2(160)
	id196 = maxId.AddPow2(196)
)

// Testing id comparison.
func TestCompare(t *testing.T) {
	assert.Equal(t, 1, id160.Compare(id128))
	assert.Equal(t, 0, id160.Compare(id160))
	assert.Equal(t, -1, id160.Compare(id196))
	assert.True(t, id160.Greater(id128))
	assert.True(t, id160.GreaterOrEqual(id128))
	assert.True(t, id160.GreaterOrEqual(id160))
	assert.True(t, id160.LesserOrEqual(id160))
	assert.True(t, id160.LesserOrEqual(id196))
	assert.True(t, id160.Lesser(id196))
}

// Testing id cyclic range membership with `BetweenII`
func TestBetweenII(t *testing.T) {
	assert.False(t, id128.BetweenII(id160, id196))
	assert.True(t, id160.BetweenII(id160, id196))
	assert.True(t, id196.BetweenII(id160, id196))
}

// Testing id cyclic range membership with `BetweenII` and inverted range
func TestBetweenII_inverted(t *testing.T) {
	assert.True(t, id128.BetweenII(id196, id160))
	assert.True(t, id160.BetweenII(id196, id160))
	assert.True(t, id196.BetweenII(id196, id160))
}

// Testing id cyclic range membership with `BetweenII` and minimal range
func TestBetweenII_minimalRange(t *testing.T) {
	assert.False(t, id128.BetweenII(id160, id160))
	assert.True(t, id160.BetweenII(id160, id160))
	assert.False(t, id196.BetweenII(id160, id160))
}

// Testing id cyclic range membership with `BetweenEE`
func TestBetweenEE(t *testing.T) {
	assert.False(t, id128.BetweenEE(id160, id196))
	assert.False(t, id160.BetweenEE(id160, id196))
	assert.False(t, id196.BetweenEE(id160, id196))
}

// Testing id cyclic range membership with `BetweenEE` and inverted range
func TestBetweenEE_inverted(t *testing.T) {
	assert.True(t, id128.BetweenEE(id196, id160))
	assert.False(t, id160.BetweenEE(id196, id160))
	assert.False(t, id196.BetweenEE(id196, id160))
}

// Testing id cyclic range membership with `BetweenEE` and minimal range
func TestBetweenEE_minimalRange(t *testing.T) {
	assert.False(t, id128.BetweenEE(id160, id160))
	assert.False(t, id160.BetweenEE(id160, id160))
	assert.False(t, id196.BetweenEE(id160, id160))
}

// Testing id cyclic range membership with `BetweenEI`
func TestBetweenEI(t *testing.T) {
	assert.False(t, id128.BetweenEI(id160, id196))
	assert.False(t, id160.BetweenEI(id160, id196))
	assert.True(t, id196.BetweenEI(id160, id196))
}

// Testing id cyclic range membership with `BetweenEI` and inverted range
func TestBetweenEI_inverted(t *testing.T) {
	assert.True(t, id128.BetweenEI(id196, id160))
	assert.True(t, id160.BetweenEI(id196, id160))
	assert.False(t, id196.BetweenEI(id196, id160))
}

// Testing id cyclic range membership with `BetweenEI` and minimal range
func TestBetweenEI_minimalRange(t *testing.T) {
	assert.False(t, id128.BetweenEI(id160, id160))
	assert.False(t, id160.BetweenEI(id160, id160))
	assert.False(t, id196.BetweenEI(id160, id160))
}

// Testing id cyclic range membership with `BetweenIE`
func TestBetweenIE(t *testing.T) {
	assert.False(t, id128.BetweenIE(id160, id196))
	assert.True(t, id160.BetweenIE(id160, id196))
	assert.False(t, id196.BetweenIE(id160, id196))
}

// Testing id cyclic range membership with `BetweenIE` and inverted range
func TestBetweenIE_inverted(t *testing.T) {
	assert.True(t, id128.BetweenIE(id196, id160))
	assert.False(t, id160.BetweenIE(id196, id160))
	assert.True(t, id196.BetweenIE(id196, id160))
}

// Testing id cyclic range membership with `BetweenIE` and minimal range
func TestBetweenIE_minimalRange(t *testing.T) {
	assert.False(t, id128.BetweenIE(id160, id160))
	assert.False(t, id160.BetweenIE(id160, id160))
	assert.False(t, id196.BetweenIE(id160, id160))
}

func TestSubtract(t *testing.T) {
	test := func(id1Hex string, id2Hex string, expected string) {
		id1, _ := MakeIdHex(id1Hex)
		id2, _ := MakeIdHex(id2Hex)
		actual := id1.Subtract(id2).ToHex()
		t.Logf("%s - %s -> %s/%s\n", id1.ToHex(), id2.ToHex(), expected, actual)
		assert.Equal(t, expected, actual)
	}

	test(
		"ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff",
		"0000000000000000000000000000000000000000000000000000000000000001",
		"fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe",
	)
	test(
		"ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff",
		"0000000000000000000000000000000000000000000000000000000000001000",
		"ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefff",
	)
	test(
		"0000000000000000000000000000000000000000000000000000000000000000",
		"0000000000000000000000000000000000000000000000000000000000000001",
		"ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff",
	)
	test(
		"0000000000000000000000000000000000000000000000000000000000000001",
		"ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff",
		"0000000000000000000000000000000000000000000000000000000000000002",
	)
}

// Testing the finger id computation results
func TestAddPow2(t *testing.T) {
	test := func(id Id, exp int, expected string) {
		newId := id.AddPow2(exp)
		t.Logf("id%03d: %s\n", exp, newId.ToHex())
		assert.Equal(t, expected, newId.ToHex())
	}

	test(maxId, 0, "0000000000000000000000000000000000000000000000000000000000000000")
	test(maxId, 1, "0000000000000000000000000000000000000000000000000000000000000001")
	test(maxId, 2, "0000000000000000000000000000000000000000000000000000000000000003")
	test(maxId, 3, "0000000000000000000000000000000000000000000000000000000000000007")
	test(maxId, 8, "00000000000000000000000000000000000000000000000000000000000000ff")
	test(maxId, 9, "00000000000000000000000000000000000000000000000000000000000001ff")
	test(maxId, 254, "3fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff")
	test(maxId, 255, "7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff")
}

// Testing the finger index computation results
func TestLog2FlooredPlusOne(t *testing.T) {
	test := func(idHex string, expected int) {
		id, _ := MakeIdHex(idHex)
		actual := id.Log2FlooredPlusOne()
		t.Logf("%s -> %d/%d\n", id.ToHex(), expected, actual)
		assert.Equal(t, expected, actual)
	}

	test("0000000000000000000000000000000000000000000000000000000000000000", 0)
	test("0000000000000000000000000000000000000000000000000000000000000001", 1)
	test("0000000000000000000000000000000000000000000000000000000000000003", 2)
	test("0000000000000000000000000000000000000000000000000000000000000007", 3)
	test("00000000000000000000000000000000000000000000000000000000000000ff", 8)
	test("00000000000000000000000000000000000000000000000000000000000001ff", 9)
	test("3fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff", 254)
	test("7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff", 255)
	test("0000000000000000000000000000000000000000000000000000000000000100", 9)
	test("0000000000000000000000000000000000000000000000000000000000000101", 9)
	test("0000000000000000000000000000000000000000000000000000000000000200", 10)
}

func TestDebug(t *testing.T) {
	// n := 5
	// t.Logf("%08b/%X -> %f\n", n, n, math.Log2(float64(n)))
	// n = 1 * 256
	// t.Logf("%08b/%X -> %f\n", n, n, math.Log2(float64(n)))
	// n = 1*256 + 1
	// t.Logf("%08b/%X -> %f\n", n, n, math.Log2(float64(n)))

	id := MakeId("localhost:8000")
	t.Logf("%s\n", id.ToBase64())
	t.Logf("%s\n", id.ToHex())
	t.Logf("%s\n", id.ToBin())

	// id, _ := MakeIdHex("9509069e25fb639d096ae409018b951b7cd60477abf43f19cd5a9b676183c3e4")
	// t.Logf("%s\n", id.ToBase64())
	// t.Logf("%s\n", id.ToHex())
	// t.Logf("%s\n", id.ToBin())
}
