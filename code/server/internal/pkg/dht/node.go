package dht

import (
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/wude/training/swo-dht/pkg/swo"
)

type LocalNodeBase struct {
	NodeInfo              // Info of the local node
	JoinNodeInfo NodeInfo // Info of the remote node to join
	Port         int32
	nodeName     string
	dataDirpath  string
	krw          swo.KeyedRWMutex // A keyed read-write-mutex used for access control to the node values
}

func (n *LocalNodeBase) Init(algorithm string) error {
	n.krw.Init()
	n.Port = n.NodeInfo.InitByEnvAndGetPort("DHT_LOCAL_NODE_DOMAIN", "DHT_LOCAL_NODE_PORT")
	_ = n.JoinNodeInfo.InitByEnvAndGetPort("DHT_REMOTE_NODE_DOMAIN", "DHT_REMOTE_NODE_PORT")
	n.nodeName = fmt.Sprintf("%s-node-%d", algorithm, n.Port)
	n.dataDirpath = swo.GetEnvOrDefault("DHT_DATA_DIR", "./data")
	return os.MkdirAll(filepath.Join(n.dataDirpath, n.nodeName), 0644)
}

func (n *LocalNodeBase) KeyValueDirpath() string {
	return filepath.Join(n.dataDirpath, n.nodeName)
}

func (n *LocalNodeBase) keyValueFilepath(id Id) string {
	return filepath.Join(n.dataDirpath, n.nodeName, id.ToHex())
}

// Store the value for the id on the local node.
// Premise is that the local node is actually responsible.
func (n *LocalNodeBase) SetValue(id Id, value string) (NodeInfo, error) {
	rw := n.krw.Lock(id)
	defer rw.Unlock()

	filename := n.keyValueFilepath(id)
	file, err := os.Create(filename)
	if err != nil {
		return n.NodeInfo, err
	}
	defer file.Close()
	file.WriteString(value)

	return n.NodeInfo, nil
}

// Delete the value for the id from the local node.
// Premise is that the local node is actually responsible.
func (n *LocalNodeBase) DelValue(id Id) (NodeInfo, error) {
	rw := n.krw.Lock(id)
	defer rw.Unlock()

	filename := n.keyValueFilepath(id)
	err := os.Remove(filename)
	if err != nil && !os.IsNotExist(err) {
		return n.NodeInfo, err
	}
	return n.NodeInfo, nil
}

// Load the value for the id from the local node.
// Premise is that the local node is actually responsible.
func (n *LocalNodeBase) GetValue(id Id) (NodeInfo, string, bool, error) {
	rw := n.krw.RLock(id)
	defer rw.RUnlock()

	filename := n.keyValueFilepath(id)
	data, err := os.ReadFile(filename)
	if err != nil {
		if os.IsNotExist(err) {
			err = nil
		}
		return n.NodeInfo, "", false, err
	}
	return n.NodeInfo, string(data), true, nil
}
