package dht

import (
	"bytes"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"
	"math/bits"
)

const (
	IdBitCount       = 256 // SHA256
	IdBitUpperIndex  = IdBitCount - 1
	IdByteCount      = IdBitCount / 8
	IdByteUpperIndex = IdByteCount - 1
)

type Id struct {
	data [IdByteCount]byte
}

var (
	// The minimum id
	minId = Id{}
	// The maximum id
	maxId = Id{data: [IdByteCount]byte{
		255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
		255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	}}
)

// The minimum id
func MinId() Id { return minId }

// The maximum id
func MaxId() Id { return maxId }

// Make an Id from a key string.
func MakeId(key string) Id {
	return Id{sha256.Sum256([]byte(key))}
}

// Make an Id from a byte slice of size `IdByteCount`.
func MakeIdSlice(src []byte) Id {
	var arr [IdByteCount]byte
	copy(arr[:], src[:])

	return Id{arr}
}

// Make an Id from a base64 encoded string.
func MakeIdBase64(src string) (Id, error) {
	slice, err := base64.URLEncoding.DecodeString(src)
	if err != nil {
		return Id{}, err
	}
	if len(slice) != IdByteCount {
		return Id{}, errors.New("id size invalid")
	}
	return MakeIdSlice(slice), err
}

// Make an Id from a hex encoded string.
func MakeIdHex(src string) (Id, error) {
	slice, err := hex.DecodeString(src)
	if err != nil {
		return Id{}, err
	}
	if len(slice) != IdByteCount {
		return Id{}, errors.New("id size invalid")
	}
	return MakeIdSlice(slice), err
}

// Make an Id from a base64 or hex encoded string.
func MakeIdString(src string) (Id, error) {
	slice, err := base64.URLEncoding.DecodeString(src)
	if len(slice) != IdByteCount || err != nil {
		slice, err = hex.DecodeString(src)
		if err != nil {
			return Id{}, err
		}
	}
	if len(slice) != IdByteCount {
		return Id{}, errors.New("id size invalid")
	}
	return MakeIdSlice(slice), err
}

// Convert an Id to a byte slice.
func (id Id) ToByteSlice() []byte { return id.data[:] }

// Convert an Id to a base64 encoded string.
func (id Id) ToBase64() string { return base64.URLEncoding.EncodeToString(id.data[:]) }

// Convert an Id to a hexadecimal encoded string.
func (id Id) ToHex() string { return hex.EncodeToString(id.data[:]) }

// Convert an Id to a binary encoded string.
func (id Id) ToBin() (str string) {
	for _, b := range id.data {
		str += fmt.Sprintf(" %08b", b)
	}
	return
}

func (id Id) Compare(other Id) int { return bytes.Compare(id.data[:], other.data[:]) }

func (id Id) Equal(other Id) bool { return bytes.Equal(id.data[:], other.data[:]) }

func (id Id) Greater(other Id) bool { return id.Compare(other) > 0 }

func (id Id) GreaterOrEqual(other Id) bool { return id.Compare(other) >= 0 }

func (id Id) Lesser(other Id) bool { return id.Compare(other) < 0 }

func (id Id) LesserOrEqual(other Id) bool { return id.Compare(other) <= 0 }

// Is the id between the interval, including both borders `[lower, upper]`?
// The lower border must not be above the upper border!
func (id Id) betweenII(lower Id, upper Id) bool {
	return id.GreaterOrEqual(lower) && id.LesserOrEqual(upper)
}

// Is the id in between the interval, excluding both borders `(lower, upper)`?
// The lower border must not be above the upper border!
func (id Id) betweenEE(lower Id, upper Id) bool {
	return id.Greater(lower) && id.Lesser(upper)
}

// Is the id between the interval, excluding the upper border `[lower, upper)`?
// The lower border must not be above the upper border!
func (id Id) betweenIE(lower Id, upper Id) bool {
	return id.GreaterOrEqual(lower) && id.Lesser(upper)
}

// Is the id between the interval, excluding the lower border `(lower, upper]`?
// The lower border must not be above the upper border!
func (id Id) betweenEI(lower Id, upper Id) bool {
	return id.Greater(lower) && id.LesserOrEqual(upper)
}

// Is the id between the cyclic interval, including both borders `[start, end]`?
func (id Id) BetweenII(start Id, end Id) bool {
	if start.Greater(end) {
		return !id.betweenEE(end, start)
	}
	return id.betweenII(start, end)
}

// Is the id in between the cyclic interval, excluding both borders `(start, end)`?
func (id Id) BetweenEE(start Id, end Id) bool {
	if start.Greater(end) {
		return !id.betweenII(end, start)
	}
	return id.betweenEE(start, end)
}

// Is the id between the cyclic interval, excluding the ending border `[start, end)`?
func (id Id) BetweenIE(start Id, end Id) bool {
	if start.Greater(end) {
		return !id.betweenIE(end, start)
	}
	return id.betweenIE(start, end)
}

// Is the id between the cyclic interval, excluding the starting border `(start, end]`?
func (id Id) BetweenEI(start Id, end Id) bool {
	if start.Greater(end) {
		return !id.betweenEI(end, start)
	}
	return id.betweenEI(start, end)
}

// Subtract another id from this id.
func (id Id) Subtract(otherId Id) (newId Id) {
	copy(newId.data[:], id.data[:])

	var bOverflow byte

	// The results of SHA2 use big endian byte order,
	// therefore the offset has to be reversed.
	for offset := IdByteUpperIndex; offset >= 0; offset-- {
		bOld := newId.data[offset]
		newId.data[offset] = bOld - otherId.data[offset] - bOverflow
		if bOld >= otherId.data[offset]-bOverflow {
			// No overflow
			bOverflow = 0
		} else {
			bOverflow = 1
		}
	}
	return
}

// Computes a finger id; LaTeX-Formula: `(id + 2^{idBitIndex}) \bmod IdBitCount`
func (id Id) AddPow2(idBitIndex int) (newId Id) {
	if idBitIndex < 0 {
		panic("id bit below range")
	}
	if idBitIndex > IdBitUpperIndex {
		panic("id bit above range")
	}
	copy(newId.data[:], id.data[:])

	// The results of SHA2 use big endian byte order,
	// therefore the offset has to be reversed.
	offset := idBitIndex / 8
	e := idBitIndex - (offset * 8)
	offset = IdByteUpperIndex - offset
	bOld := newId.data[offset]
	bNew := bOld + (1 << e) // Adding the binary exponentiation result inside the significant byte

	for {
		newId.data[offset] = bNew
		if bOld < bNew {
			// No overflow
			break
		}
		offset--
		if offset < 0 {
			// Overflow is to be ignored
			// Masking the overflow acts as binary modulo
			break
		}
		// Applying overflow to the next byte
		bOld = newId.data[offset]
		bNew = bOld + 1
	}
	return
}

// Floored binary logarithm of the given id;
// The result value `0` is a special case when the id is full of zeros (see `minId`).
// This case should normally result in negative infinity.
// LaTeX-Formula: `\lfloor \log_2 id \rfloor + 1`.
func (id Id) Log2FlooredPlusOne() (e int) {
	for i, b := range id.data {
		// Count the required bits for that byte
		bLen := bits.Len8(b)
		e += bLen
		if bLen > 0 {
			// At least one bit is required for this byte,
			// now add full bit count for the remaining bytes.
			e += (IdByteUpperIndex - i) * 8
			break
		}
	}
	return
}
