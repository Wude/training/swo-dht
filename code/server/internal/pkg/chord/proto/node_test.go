package proto

import (
	"context"
	"net"
	"testing"
	"time"

	"gitlab.com/wude/training/swo-dht/internal/pkg/dht"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

const (
	// address = ":8000"
	address = "127.0.0.1:8000"
)

type testServer struct {
	NodeServer
	t *testing.T
}

type testClient struct {
	NodeClient
	conn *grpc.ClientConn
}

func (s *testServer) Successor(ctx context.Context, req *Void) (*NodeInfo, error) {
	s.t.Logf("Successor")
	return &NodeInfo{}, nil
}

func (s *testServer) Predecessor(ctx context.Context, req *Void) (*NodeInfo, error) {
	s.t.Logf("Predecessor")
	return &NodeInfo{}, nil
}

func (s *testServer) FindSuccessor(ctx context.Context, req *Id) (*NodeAndHopInfo, error) {
	s.t.Logf("FindSuccessor: Id = %s", dht.MakeIdSlice(req.GetId()).ToHex())
	return &NodeAndHopInfo{}, nil
}

func (s *testServer) FindPredecessor(ctx context.Context, req *Id) (*NodeAndHopInfo, error) {
	s.t.Logf("FindPredecessor: Id = %s", dht.MakeIdSlice(req.GetId()).ToHex())
	return &NodeAndHopInfo{}, nil
}

func (s *testServer) ClosestPrecedingFinger(ctx context.Context, req *Id) (*NodeInfo, error) {
	s.t.Logf("ClosestPrecedingFinger: Id = %s", dht.MakeIdSlice(req.GetId()).ToHex())
	return &NodeInfo{}, nil
}

func (s *testServer) NotificationByPredecessor(ctx context.Context, req *NodeInfo) (*Void, error) {
	s.t.Logf("NotificationByPredecessor: PredecessorId = %v; PredecessorUrl = %v", req.GetId(), req.GetUrl())
	return &Void{}, nil
}

func (s *testServer) SetValue(ctx context.Context, req *IdAndValue) (*NodeAndHopInfo, error) {
	s.t.Logf("SetValue: Id = %s", dht.MakeIdSlice(req.GetId()).ToHex())
	return &NodeAndHopInfo{}, nil
}

func (s *testServer) DelValue(ctx context.Context, req *Id) (*NodeAndHopInfo, error) {
	s.t.Logf("DelValue: Id = %s", dht.MakeIdSlice(req.GetId()).ToHex())
	return &NodeAndHopInfo{}, nil
}

func (s *testServer) GetValue(ctx context.Context, req *Id) (*ValueInfo, error) {
	s.t.Logf("GetValue: Id = %s", dht.MakeIdSlice(req.GetId()).ToHex())
	return &ValueInfo{Value: "1349", ResponsibleNode: &NodeAndHopInfo{}}, nil
}

func startTestServer(t *testing.T) (*grpc.Server, error) {
	lis, err := net.Listen("tcp", address)
	if err != nil {
		return nil, err
	}
	srv := grpc.NewServer()
	RegisterNodeServer(srv, &testServer{t: t})
	go func() { _ = srv.Serve(lis) }()

	return srv, nil
}

func runServer(t *testing.T, action func(t *testing.T)) {
	s, err := startTestServer(t)

	if err != nil {
		t.Fatalf("server startup failed: %v", err)
	} else {
		action(t)
	}

	s.GracefulStop()
}

func makeTestClient(t *testing.T) testClient {
	conn, err := grpc.Dial(address, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		t.Fatalf("did not connect: %v", err)
	}
	return testClient{NewNodeClient(conn), conn}
}

func (c testClient) close() {
	c.conn.Close()
}

func callGetValue(t *testing.T) {
	c := makeTestClient(t)
	defer c.close()
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	res, err := c.GetValue(ctx, &Id{Id: dht.MakeId("123").ToByteSlice()})
	if err != nil {
		t.Fatalf("could not get value: %v", err)
	}
	t.Logf("value: \"%s\"", res.GetValue())
}

// One test gRPC call to see if the connection and wiring works.
func TestServer(t *testing.T) {
	runServer(t, func(t *testing.T) {
		callGetValue(t)
	})
}
