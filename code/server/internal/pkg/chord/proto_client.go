package chord

import (
	"context"
	"time"

	"gitlab.com/wude/training/swo-dht/internal/pkg/chord/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type protoClient struct {
	proto.NodeClient
	conn   *grpc.ClientConn
	ctx    context.Context
	cancel context.CancelFunc
}

func MakeNodeClient(url string, timeout time.Duration) (protoClient, error) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	conn, err := grpc.DialContext(ctx, url, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		cancel()
		return protoClient{}, err
	}
	return protoClient{proto.NewNodeClient(conn), conn, ctx, cancel}, nil
}

func (c protoClient) Close() {
	_ = c.conn.Close()
	c.cancel()
}
