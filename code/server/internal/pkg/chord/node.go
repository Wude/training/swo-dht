package chord

import "gitlab.com/wude/training/swo-dht/internal/pkg/dht"

type Node interface {
	// Get the id of this node.
	Id() dht.Id

	// Get the url of this node.
	Url() string

	// Get successor node of this node.
	Successor() (node Node, unavailable bool, err error)

	// Get predecessor node of this node.
	Predecessor() (node Node, unavailable bool, err error)

	// Find the successor node for the given id.
	// This node is responsible for the id.
	FindSuccessor(id dht.Id) (node Node, hopCount uint32, unavailable bool, err error)

	// Find the predecessor node for the given id.
	FindPredecessor(id dht.Id) (node Node, hopCount uint32, unavailable bool, err error)

	// Get the node from the fingers, which is closest in preceding the given id.
	ClosestPrecedingFinger(id dht.Id) (node Node, unavailable bool, err error)

	// Receive a notification by a node which claims to be the predecessor of this node.
	NotificationByPredecessor(predecessor Node) (unavailable bool, err error)

	// Receive a notification by a node which confirms to be the successor of this node.
	NotificationBySuccessor(successor Node) (unavailable bool, err error)

	// Check the given node for availability.
	CheckNode(node Node) (unavailable bool, err error)

	// Set a replicated value for the given id.
	SetReplicatedValue(id dht.Id, value string) (unavailable bool, err error)

	// Set the value for the given id.
	SetValue(id dht.Id, value string) (nodeInfo dht.NodeInfo, hopCount uint32, unavailable bool, err error)

	// Delete the value for the given id.
	DelValue(id dht.Id) (nodeInfo dht.NodeInfo, hopCount uint32, unavailable bool, err error)

	// Get the value for the given id.
	GetValue(id dht.Id) (nodeInfo dht.NodeInfo, hopCount uint32, value string, found bool, unavailable bool, err error)
}
