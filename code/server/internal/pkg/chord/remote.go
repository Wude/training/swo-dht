package chord

import (
	"context"
	"time"

	"gitlab.com/wude/training/swo-dht/internal/pkg/chord/proto"
	"gitlab.com/wude/training/swo-dht/internal/pkg/dht"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const remoteCallTimeout = 2 * time.Second

var voidInfo *proto.Void = &proto.Void{}

type remoteNode struct {
	dht.NodeInfo
	client protoClient
	local  *LocalNode
}

var _ Node = &remoteNode{} // Force implementation check

func NewRemoteNode(nodeInfo dht.NodeInfo, local *LocalNode) (*remoteNode, error) {
	client, err := MakeNodeClient(nodeInfo.Url(), remoteCallTimeout)
	if err != nil {
		return nil, err
	}
	n := &remoteNode{NodeInfo: nodeInfo, client: client, local: local}
	return n, nil
}

func (n remoteNode) Close() {
	n.client.Close()
}

func (n *remoteNode) getNode(nodeInfo *proto.NodeInfo) (Node, error) {
	internalNodeInfo, err := dht.MakeNodeInfo(nodeInfo.Id, nodeInfo.Url)
	if err != nil {
		return nil, err
	}
	return n.local.getNode(internalNodeInfo)
}

func remoteCall[TRequest any, TResponse any](
	action func(context.Context, *TRequest, ...grpc.CallOption) (*TResponse, error),
	req *TRequest,
) (
	*TResponse, bool, error,
) {
	ctx, cancel := context.WithTimeout(context.Background(), remoteCallTimeout)
	defer cancel()

	var unavailable bool
	result, err := action(ctx, req)
	if err != nil {
		errStatus, ok := status.FromError(err)
		if ok {
			statusCode := errStatus.Code()
			// switch statusCode {
			// case codes.DeadlineExceeded:
			// case codes.Unavailable:
			// 	unavailable = true
			// }
			unavailable = statusCode != codes.OK
		}
	}
	// return result, ctx.Err() == context.Canceled, err
	return result, unavailable, err
}

func getNodeCall[TRequest any](
	n *remoteNode,
	action func(context.Context, *TRequest, ...grpc.CallOption) (*proto.NodeInfo, error),
	req *TRequest,
) (
	Node, bool, error,
) {
	res, unavailable, err := remoteCall(action, req)
	if err != nil || unavailable {
		return nil, unavailable, err
	}
	node, err := n.getNode(res)
	return node, unavailable, err
}

func getNodeAndHopInfoCall(
	n *remoteNode,
	action func(context.Context, *proto.Id, ...grpc.CallOption) (*proto.NodeAndHopInfo, error),
	id dht.Id,
) (
	Node, uint32, bool, error,
) {
	res, unavailable, err := remoteCall(action, &proto.Id{Id: id.ToByteSlice()})
	if err != nil || unavailable {
		return nil, 0, unavailable, err
	}
	node, err := n.getNode(res.Node)
	return node, res.HopCount, unavailable, err
}

func (n *remoteNode) Successor() (Node, bool, error) {
	return getNodeCall(n, n.client.Successor, voidInfo)
}

func (n *remoteNode) Predecessor() (Node, bool, error) {
	return getNodeCall(n, n.client.Predecessor, voidInfo)
}

func (n *remoteNode) FindSuccessor(id dht.Id) (Node, uint32, bool, error) {
	return getNodeAndHopInfoCall(n, n.client.FindSuccessor, id)
}

func (n *remoteNode) FindPredecessor(id dht.Id) (Node, uint32, bool, error) {
	return getNodeAndHopInfoCall(n, n.client.FindPredecessor, id)
}

func (n *remoteNode) ClosestPrecedingFinger(id dht.Id) (Node, bool, error) {
	return getNodeCall(n, n.client.ClosestPrecedingFinger, &proto.Id{Id: id.ToByteSlice()})
}

func (n *remoteNode) NotificationByPredecessor(predecessor Node) (bool, error) {
	_, unavailable, err := remoteCall(n.client.NotificationByPredecessor, &proto.NodeInfo{
		Id:  predecessor.Id().ToByteSlice(),
		Url: predecessor.Url(),
	})
	return unavailable, err
}

func (n *remoteNode) NotificationBySuccessor(predecessor Node) (unavailable bool, err error) {
	_, unavailable, err = remoteCall(n.client.NotificationBySuccessor, &proto.NodeInfo{
		Id:  predecessor.Id().ToByteSlice(),
		Url: predecessor.Url(),
	})
	return
}

// Check the given node for availability.
func (n *remoteNode) CheckNode(node Node) (unavailable bool, err error) {
	_, unavailable, err = remoteCall(n.client.CheckNode, &proto.NodeInfo{
		Id:  node.Id().ToByteSlice(),
		Url: node.Url(),
	})
	return
}

// Set a replicated value for the given id.
func (n *remoteNode) SetReplicatedValue(id dht.Id, value string) (bool, error) {
	_, unavailable, err := remoteCall(n.client.SetReplicatedValue, &proto.IdAndValue{
		Id:    id.ToByteSlice(),
		Value: value,
	})
	return unavailable, err
}

func (n *remoteNode) SetValue(id dht.Id, value string) (dht.NodeInfo, uint32, bool, error) {
	res, unavailable, err := remoteCall(n.client.SetValue, &proto.IdAndValue{
		Id:    id.ToByteSlice(),
		Value: value,
	})
	if err != nil {
		return dht.NodeInfo{}, 0, unavailable, err
	}
	return n.NodeInfo, res.HopCount, unavailable, err
}

func (n *remoteNode) DelValue(id dht.Id) (dht.NodeInfo, uint32, bool, error) {
	res, unavailable, err := remoteCall(n.client.DelValue, &proto.Id{
		Id: id.ToByteSlice(),
	})
	if err != nil {
		return dht.NodeInfo{}, 0, unavailable, err
	}
	return n.NodeInfo, res.HopCount, unavailable, err
}

func (n *remoteNode) GetValue(id dht.Id) (dht.NodeInfo, uint32, string, bool, bool, error) {
	res, unavailable, err := remoteCall(n.client.GetValue, &proto.Id{
		Id: id.ToByteSlice(),
	})
	if err == nil {
		nodeInfo, err := dht.MakeNodeInfo(
			res.ResponsibleNode.Node.Id,
			res.ResponsibleNode.Node.Url)
		if err == nil {
			return nodeInfo, res.ResponsibleNode.HopCount, res.Value, res.Found, unavailable, nil
		}
	}
	return dht.NodeInfo{}, 0, "", false, unavailable, err
}
