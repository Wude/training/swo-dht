package chord

import (
	"context"

	log "github.com/sirupsen/logrus"
	"gitlab.com/wude/training/swo-dht/internal/pkg/chord/proto"
	"gitlab.com/wude/training/swo-dht/internal/pkg/dht"
	"google.golang.org/grpc"
)

type protoServer struct {
	proto.NodeServer
	grpc  *grpc.Server
	local *LocalNode
}

var _ proto.NodeServer = &protoServer{}

func (s *protoServer) init(node *LocalNode) {
	s.grpc = grpc.NewServer()
	s.local = node
	proto.RegisterNodeServer(s.grpc, s)
}

func (s *protoServer) Successor(context.Context, *proto.Void) (*proto.NodeInfo, error) {
	log.Tracef("proto.Successor")

	res := &proto.NodeInfo{}
	node, _, err := s.local.Successor()
	if err == nil {
		res.Id = node.Id().ToByteSlice()
		res.Url = node.Url()
	}
	return res, err
}

func (s *protoServer) Predecessor(context.Context, *proto.Void) (*proto.NodeInfo, error) {
	log.Tracef("proto.Predecessor")

	res := &proto.NodeInfo{}
	node, _, err := s.local.Predecessor()
	if err == nil && node != nil {
		res.Id = node.Id().ToByteSlice()
		res.Url = node.Url()
	}
	return res, err
}

func (s *protoServer) FindSuccessor(ctx context.Context, req *proto.Id) (*proto.NodeAndHopInfo, error) {
	log.Tracef("proto.FindSuccessor: Id = %v", req.Id)

	res := &proto.NodeAndHopInfo{Node: &proto.NodeInfo{}}
	id := dht.MakeIdSlice(req.Id)
	node, hopCount, _, err := s.local.FindSuccessor(id)
	if err == nil {
		res.Node.Id = node.Id().ToByteSlice()
		res.Node.Url = node.Url()
		res.HopCount = hopCount
	}
	return res, err
}

func (s *protoServer) FindPredecessor(ctx context.Context, req *proto.Id) (*proto.NodeAndHopInfo, error) {
	log.Tracef("proto.FindPredecessor: Id = %v", req.Id)

	res := &proto.NodeAndHopInfo{Node: &proto.NodeInfo{}}
	id := dht.MakeIdSlice(req.Id)
	node, hopCount, _, err := s.local.FindPredecessor(id)
	if err == nil {
		res.Node.Id = node.Id().ToByteSlice()
		res.Node.Url = node.Url()
		res.HopCount = hopCount
	}
	return res, err
}

func (s *protoServer) ClosestPrecedingFinger(ctx context.Context, req *proto.Id) (*proto.NodeInfo, error) {
	log.Tracef("proto.ClosestPrecedingFinger: Id = %v", req.Id)

	res := &proto.NodeInfo{}
	id := dht.MakeIdSlice(req.Id)
	node, _, err := s.local.ClosestPrecedingFinger(id)
	if err == nil {
		res.Id = node.Id().ToByteSlice()
		res.Url = node.Url()
	}
	return res, err
}

func (s *protoServer) NotificationByPredecessor(ctx context.Context, req *proto.NodeInfo) (*proto.Void, error) {
	log.Tracef("proto.NotificationByPredecessor: PredecessorUrl = %v", req.GetUrl())

	nodeInfo, err := dht.MakeNodeInfo(req.Id, req.Url)
	if err != nil {
		log.Fatal(err.Error())
	}
	node, err := s.local.getNode(nodeInfo)
	if err != nil {
		log.Error(err.Error())
	}
	_, err = s.local.NotificationByPredecessor(node)
	return &proto.Void{}, err
}

func (s *protoServer) NotificationBySuccessor(ctx context.Context, req *proto.NodeInfo) (*proto.Void, error) {
	log.Tracef("proto.NotificationBySuccessor: SuccessorUrl = %v", req.GetUrl())

	nodeInfo, err := dht.MakeNodeInfo(req.Id, req.Url)
	if err != nil {
		log.Fatal(err.Error())
	}
	node, err := s.local.getNode(nodeInfo)
	if err != nil {
		log.Error(err.Error())
	}
	_, err = s.local.NotificationBySuccessor(node)
	return &proto.Void{}, err
}

func (s *protoServer) CheckNode(ctx context.Context, req *proto.NodeInfo) (*proto.Void, error) {
	log.Tracef("proto.CheckNode: NodeUrl = %v", req.GetUrl())

	nodeInfo, err := dht.MakeNodeInfo(req.Id, req.Url)
	if err != nil {
		log.Fatal(err.Error())
	}
	node, err := s.local.getNode(nodeInfo)
	if err != nil {
		log.Error(err.Error())
	}
	_, err = s.local.CheckNode(node)
	return &proto.Void{}, err
}

func (s *protoServer) SetReplicatedValue(ctx context.Context, req *proto.IdAndValue) (*proto.Void, error) {
	log.Tracef("proto.SetReplicatedValue: Id = %v", req.GetId())

	id := req.GetId()
	value := req.GetValue()
	_, err := s.local.SetReplicatedValue(dht.MakeIdSlice(id), value)
	return &proto.Void{}, err
}

func (s *protoServer) SetValue(ctx context.Context, req *proto.IdAndValue) (*proto.NodeAndHopInfo, error) {
	log.Tracef("proto.SetValue: Id = %v", req.GetId())

	id := req.GetId()
	value := req.GetValue()
	node, hopCount, _, err := s.local.SetValue(dht.MakeIdSlice(id), value)
	return &proto.NodeAndHopInfo{
			Node: &proto.NodeInfo{
				Id:  node.Id().ToByteSlice(),
				Url: node.Url()},
			HopCount: hopCount},
		err
}

func (s *protoServer) DelValue(ctx context.Context, req *proto.Id) (*proto.NodeAndHopInfo, error) {
	log.Tracef("proto.DelValue: Id = %v", req.GetId())

	id := req.GetId()
	node, hopCount, _, err := s.local.DelValue(dht.MakeIdSlice(id))
	return &proto.NodeAndHopInfo{
			Node: &proto.NodeInfo{
				Id:  node.Id().ToByteSlice(),
				Url: node.Url()},
			HopCount: hopCount},
		err
}

func (s *protoServer) GetValue(ctx context.Context, req *proto.Id) (*proto.ValueInfo, error) {
	log.Tracef("proto.GetValue: Id = %v", req.GetId())

	res := &proto.ValueInfo{}
	node, hopCount, value, found, _, err := s.local.GetValue(dht.MakeIdSlice(req.GetId()))
	if err == nil {
		res.Value = value
		res.Found = found
		res.ResponsibleNode = &proto.NodeAndHopInfo{
			Node: &proto.NodeInfo{
				Id:  node.Id().ToByteSlice(),
				Url: node.Url()},
			HopCount: hopCount}
	}
	return res, err
}
