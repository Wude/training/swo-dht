package chord

import (
	// "io/ioutil"

	log "github.com/sirupsen/logrus"
	"gitlab.com/wude/training/swo-dht/internal/pkg/dht"
)

type finger struct {
	successor     Node
	intervalStart dht.Id
}

func (n *LocalNode) initFingers() {
	n.nextFingerIndex = 0
	n.knownNodesByUrl = make(map[string]*remoteNode)

	for i := range n.fingers {
		id := n.Id().AddPow2(i)
		n.fingers[i].successor = n
		n.fingers[i].intervalStart = id
	}

	// Everything from including `n.NodeId` until excluding
	// `n.fingers[0].intervalStart` will be in responsibility of the local node.
	// All other ranges must be handled by using fingers.

	// Everything from including `n.predecessor.GetNodeId()` until excluding
	// `n.NodeId` will be in responsibility of the local node.
	// All other ranges must be handeled by using fingers.
}

func (n *LocalNode) getNode(nodeInfo dht.NodeInfo) (Node, error) {
	if n.Url() == nodeInfo.Url() {
		return n, nil
	}

	node, ok := n.knownNodesByUrl[nodeInfo.Url()]
	if !ok {
		var err error
		node, err = NewRemoteNode(nodeInfo, n)
		if err != nil {
			return nil, err
		}
		n.knownNodesByUrl[nodeInfo.Url()] = node
	}
	return node, nil
}

// Replace a node that has proven to be unreliable.
func (n *LocalNode) replaceNode(node Node) (found bool) {
	if n.predecessor == node {
		n.predecessor = nil
	}
	for i := range n.fingers {
		if n.fingers[i].successor == node {
			found = true
			n.replaceFinger(i)
		}
	}
	return
}

// Replace a finger node that has proven to be unreliable.
func (n *LocalNode) replaceFinger(fingerIndex int) {
	id := n.fingers[fingerIndex].successor.Id()
	var i int
	var successor Node

	// Get the index of a finger with another node
	for i = fingerIndex + 1; i < dht.IdBitCount; i++ {
		if !id.Equal(n.fingers[i].successor.Id()) {
			break
		}
	}

	if i == dht.IdBitCount {
		successor = n // There is no other node
	} else {
		successor = n.fingers[i].successor // Get the other node as replacement
	}

	// Copy the new successor down to the finger to replace
	for i--; i >= fingerIndex; i-- {
		n.fingers[i].successor = successor
	}
}

// Join the given node and its network.
func (n *LocalNode) join(nodeInfo dht.NodeInfo) (err error) {
	log.Tracef("LocalNode.join")
	if n.Url() != n.JoinNodeInfo.Url() {
		// log.Tracef("LocalNode.join - a")

		var joinNode Node
		if joinNode, err = n.getNode(n.JoinNodeInfo); err != nil {
			log.Errorf(err.Error())
		} else {
			var successor Node
			// log.Tracef("LocalNode.join - b")
			if successor, _, _, err = joinNode.FindSuccessor(n.Id()); err == nil {
				log.Tracef("n.fingers[0].successor = \"%s\"", successor.Url())
				n.fingers[0].successor = successor
				_, err = successor.NotificationByPredecessor(n)
			}
			n.buildFingers(joinNode)
		}
	}
	return
}

// Fix the successor.
func (n *LocalNode) stabilize() error {
	log.Tracef("LocalNode.stabilize")

	node, unavailable, err := n.fingers[0].successor.Predecessor()
	for unavailable {
		n.replaceFinger(0)
		node, unavailable, err = n.fingers[0].successor.Predecessor()
	}

	if err != nil {
		log.Info(err.Error())
	} else if node != nil {
		if n == n.fingers[0].successor ||
			n.Id().Equal(n.fingers[0].successor.Id()) ||
			node.Id().BetweenEE(n.Id(), n.fingers[0].successor.Id()) {

			n.fingers[0].successor = node
		}
	}

	unavailable, err = n.fingers[0].successor.NotificationByPredecessor(n)
	for unavailable {
		n.replaceFinger(0)
		unavailable, err = n.fingers[0].successor.NotificationByPredecessor(n)
	}
	if err != nil {
		log.Info(err.Error())
	}
	return nil
}

// Fix finger entries.
func (n *LocalNode) fixFingers() error {
	log.Tracef("LocalNode.fixFingers")

	i := n.nextFingerIndex
	n.nextFingerIndex += 1
	n.nextFingerIndex &= dht.IdBitUpperIndex
	successor, _, _, err := n.FindSuccessor(n.fingers[i].intervalStart)
	if err != nil {
		log.Errorf(err.Error())
	} else if n.fingers[i].successor != successor {
		n.fingers[i].successor = successor
	}

	// n.buildFingers(n)

	return nil
}

func (n *LocalNode) buildFingers(node Node) (err error) {
	lowerIndex := n.fingers[0].successor.Id().Subtract(n.Id()).Log2FlooredPlusOne()
	log.Tracef("LocalNode.buildFingers; lowerIndex = %d (%s - %s = %s)",
		lowerIndex,
		n.fingers[0].successor.Id().ToHex(),
		n.Id().ToHex(),
		n.fingers[0].successor.Id().Subtract(n.Id()).ToHex(),
	)

	for i := dht.IdBitUpperIndex; i >= lowerIndex; i-- {
		n.fingers[i].successor, _, _, err = node.FindSuccessor(n.fingers[i].intervalStart)
		log.Tracef("LocalNode.buildFingers; n.fingers[%d].successor = %s", i, n.fingers[i].successor.Id().ToHex())
		if err != nil {
			log.Errorf(err.Error())
		}
	}

	return nil
}
