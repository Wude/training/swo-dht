package chord

import (
	"context"
	"fmt"
	"net"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/soheilhy/cmux"
	"gitlab.com/wude/training/swo-dht/internal/pkg/dht"
	"gitlab.com/wude/training/swo-dht/pkg/swo"
)

type LocalNode struct {
	dht.LocalNodeBase

	predecessor     Node
	fingers         [dht.IdBitCount]finger
	nextFingerIndex int
	knownNodesByUrl map[string]*remoteNode

	proto protoServer
	rest  restServer
	cmux  cmux.CMux
}

var _ Node = &LocalNode{} // Force implementation check

func StartLocalNode() *LocalNode {
	n := new(LocalNode)

	err := n.LocalNodeBase.Init("chord")
	if err == nil {
		n.initFingers()
		n.proto.init(n)
		n.rest.init(n)

		var lis net.Listener
		lis, err = net.Listen("tcp", fmt.Sprintf(":%d", n.Port))
		if err == nil {
			n.cmux = cmux.New(lis)

			grpcLis := n.cmux.MatchWithWriters(cmux.HTTP2MatchHeaderFieldSendSettings("content-type", "application/grpc"))
			httpLis := n.cmux.Match(cmux.Any())

			go func() { _ = n.proto.grpc.Serve(grpcLis) }()
			go func() { _ = n.cmux.Serve() }()

			if err := n.join(n.JoinNodeInfo); err != nil {
				log.Error(err.Error())
			}
			go func() { _ = n.rest.http.Serve(httpLis) }()

			_, _ = swo.RunPeriodically(10*time.Second, n.stabilize)
			_, _ = swo.RunPeriodically(5*time.Second, n.fixFingers)
			log.Infof("Node started with url \"%s\".", n.Url())

			return n
		}
	}
	log.Fatal("could not startup local node:\n", err)
	return nil
}

func (n *LocalNode) Stop() error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	for _, node := range n.knownNodesByUrl {
		node.Close()
	}

	if err := n.rest.http.Shutdown(ctx); err != nil {
		log.Fatal("rest server shutdown forced:\n", err)
	}
	n.proto.grpc.GracefulStop()
	log.Infof("Node stopped.")

	return nil
}

// Get successor node of this node.
func (n *LocalNode) Successor() (Node, bool, error) {
	log.Tracef("LocalNode.Successor")

	return n.fingers[0].successor, false, nil
}

// Get predecessor node of this node.
func (n *LocalNode) Predecessor() (Node, bool, error) {
	log.Tracef("LocalNode.Predecessor")

	return n.predecessor, false, nil
}

// Find the successor node for the given id.
// This node is responsible for the id.
func (n *LocalNode) FindSuccessor(id dht.Id) (
	successor Node,
	hopCount uint32,
	unavailable bool,
	err error,
) {
	log.Tracef("LocalNode.FindSuccessor")
	var predecessor Node

	predecessor, hopCount, _, err = n.FindPredecessor(id)
	if err == nil {
		successor, _, err = predecessor.Successor()
	}
	if successor != n {
		hopCount += 1
	} else {
		hopCount = 0
	}
	return
}

// Find the predecessor node for the given id.
func (n *LocalNode) FindPredecessor(id dht.Id) (
	predecessor Node,
	hopCount uint32,
	unavailable bool,
	err error,
) {
	log.Tracef("LocalNode.FindPredecessor")

	var successor Node
	var lastPredecessor Node = nil
	predecessor = n

	for {
		successor, unavailable, err = predecessor.Successor()
		if unavailable {
			unavailable, err = lastPredecessor.CheckNode(predecessor)
			if !unavailable {
				lastPredecessor = nil
				predecessor = n
				continue
			}
		}
		if err != nil {
			log.Errorf(err.Error())
			// Step back to the last node and use it as result.
			predecessor = lastPredecessor
			break
		}
		if !id.BetweenEI(predecessor.Id(), successor.Id()) {
			predecessor, _, err = predecessor.ClosestPrecedingFinger(id)
			if err != nil {
				log.Errorf(err.Error())
				// Step back to the last node and use it as result.
				predecessor = lastPredecessor
				break
			}
		}
		if lastPredecessor == predecessor {
			break
		}
		lastPredecessor = predecessor
		if predecessor != n {
			hopCount++
		} else {
			hopCount = 0
			break
		}
	}
	unavailable = false
	return
}

// Get the node from the fingers, which is closest in preceding the given id.
func (n *LocalNode) ClosestPrecedingFinger(id dht.Id) (Node, bool, error) {
	log.Tracef("LocalNode.ClosestPrecedingFinger")

	for i := dht.IdBitUpperIndex; i >= 0; i-- {
		if n.fingers[i].successor.Id().BetweenEE(n.Id(), id) {
			return n.fingers[i].successor, false, nil
		}
	}
	return n, false, nil
}

// Receive a notification by a node which claims to be the predecessor of this node.
func (n *LocalNode) NotificationByPredecessor(predecessor Node) (unavailable bool, err error) {
	if n != predecessor {
		log.Tracef("LocalNode.NotificationByPredecessor: \"%s\"", predecessor.Url())

		if n.predecessor == nil || predecessor.Id().BetweenEE(
			n.predecessor.Id(), n.Id(),
		) {
			n.predecessor = predecessor
			unavailable, err = predecessor.NotificationBySuccessor(n)
			if unavailable {
				n.predecessor = nil
			}
		}
	}
	unavailable = false
	return
}

// Receive a notification by a node which confirms to be the successor of this node.
func (n *LocalNode) NotificationBySuccessor(successor Node) (unavailable bool, err error) {
	if n != successor {
		log.Tracef("LocalNode.NotificationBySuccessor: \"%s\"", successor.Url())

		// List all key-value-pairs and replicate them to the new successor
		var entries []os.DirEntry
		var id dht.Id
		var value string
		entries, _ = os.ReadDir(n.LocalNodeBase.KeyValueDirpath())
		for _, entry := range entries {
			if !entry.IsDir() {
				id, err = dht.MakeIdString(entry.Name())
				// Only replicate values in our responsibility
				if err == nil && (n.predecessor == nil || id.BetweenEI(n.predecessor.Id(), n.Id())) {
					_, value, _, err = n.LocalNodeBase.GetValue(id)
					if err == nil {
						successor.SetReplicatedValue(id, value)
					}
				}
			}
		}
	}
	return
}

// Check the given node for availability.
func (n *LocalNode) CheckNode(node Node) (unavailable bool, err error) {
	// For now we assume the caller was right to point out that the given node is dead.
	_ = n.replaceNode(node)
	return
}

// Set a replicated value for the given id.
func (n *LocalNode) SetReplicatedValue(id dht.Id, value string) (unavailable bool, err error) {
	_, err = n.LocalNodeBase.SetValue(id, value)
	return
}

// Set the value for the given id.
func (n *LocalNode) SetValue(id dht.Id, value string) (dht.NodeInfo, uint32, bool, error) {
	log.Tracef("LocalNode.SetValue")

	for {
		node, hopCount, _, err := n.FindSuccessor(id)
		if err != nil {
			return n.NodeInfo, hopCount, false, err
		}
		if node == n {
			nodeInfo, err := n.LocalNodeBase.SetValue(id, value)
			if err != nil {
				_, err = n.fingers[0].successor.SetReplicatedValue(id, value)
			}
			return nodeInfo, 0, false, err
		}
		nodeInfo, hc, unavailable, err := node.SetValue(id, value)
		if !unavailable {
			return nodeInfo, hopCount + hc, false, err
		}
		n.replaceNode(node)
	}
}

// Delete the value for the given id.
func (n *LocalNode) DelValue(id dht.Id) (dht.NodeInfo, uint32, bool, error) {
	log.Tracef("LocalNode.DelValue")

	for {
		node, hopCount, _, err := n.FindSuccessor(id)
		if err != nil {
			return n.NodeInfo, hopCount, false, err
		}
		if node == n {
			nodeInfo, err := n.LocalNodeBase.DelValue(id)
			return nodeInfo, 0, false, err
		}
		nodeInfo, hc, unavailable, err := node.DelValue(id)
		if !unavailable {
			return nodeInfo, hopCount + hc, false, err
		}
		n.replaceNode(node)
	}
}

// Get the value for the given id.
func (n *LocalNode) GetValue(id dht.Id) (dht.NodeInfo, uint32, string, bool, bool, error) {
	log.Tracef("LocalNode.GetValue")

	for {
		node, hopCount, _, err := n.FindSuccessor(id)
		if err != nil {
			return n.NodeInfo, hopCount, "", false, false, err
		}
		if node == n {
			nodeInfo, value, found, err := n.LocalNodeBase.GetValue(id)
			return nodeInfo, 0, value, found, false, err
		}
		nodeInfo, hc, value, found, unavailable, err := node.GetValue(id)
		if !unavailable {
			return nodeInfo, hopCount + hc, value, found, false, err
		}
		n.replaceNode(node)
	}
}
