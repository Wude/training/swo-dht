package chord

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	docs "gitlab.com/wude/training/swo-dht/docs/chord"
	"gitlab.com/wude/training/swo-dht/internal/pkg/dht"
	"gitlab.com/wude/training/swo-dht/pkg/swo"
)

var (
	swaggerHandler = ginSwagger.WrapHandler(swaggerfiles.Handler)
)

type restServer struct {
	gin   *gin.Engine
	http  *http.Server
	local *LocalNode
}

// @BasePath /

type errorResponse struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}

type setValueRequest struct {
	Key   string `json:"key,omitempty"`
	Id    string `json:"id,omitempty"`
	Value string `json:"value"`
}

type changeResponse struct {
	NodeUrl  string `json:"nodeUrl"`
	HopCount uint32 `json:"hopCount"`
}

type getValueResponse struct {
	NodeUrl  string `json:"nodeUrl"`
	HopCount uint32 `json:"hopCount"`
	Value    string `json:"value"`
}

type fingerResponse struct {
	FingerIndex   int    `json:"fingerIndex"`
	SuccessorUrl  string `json:"successorUrl"`
	SuccessorId   string `json:"successorId"`
	IntervalStart string `json:"intervalStart"`
}

type fingerListResponse struct {
	NodeUrl        string           `json:"nodeUrl"`
	NodeId         string           `json:"nodeId"`
	PredecessorUrl string           `json:"predecessorUrl"`
	PredecessorId  string           `json:"predecessorId"`
	Fingers        []fingerResponse `json:"fingers"`
}

func returnError(ctx *gin.Context, status int, msg string) {
	ctx.JSON(status, &errorResponse{Status: status, Message: msg})
}

// Set a value by a key
// @Summary Set a value by a key
// @Schemes
// @Description Set a value by a key
// @Tags api/v1
// @Accept json
// @Produce json
// @Param setValue body setValueRequest true "Information of the value to set"
// @Success 200 {object} changeResponse
// @Failure 400 {object} errorResponse
// @Failure 404 {object} errorResponse
// @Failure 500 {object} errorResponse
// @BasePath /api/v1
// @Router /value [post] setValueRequest
func (r *restServer) setValue(ctx *gin.Context) {
	var req setValueRequest
	var id dht.Id

	if err := ctx.BindJSON(&req); err == nil {
		if len(req.Key) > 0 {
			id = dht.MakeId(req.Key)
		} else if len(req.Id) > 0 {
			id, err = dht.MakeIdString(req.Id)
			if err != nil {
				returnError(ctx, http.StatusNotFound, "id invalid")
				return
			}
		} else {
			returnError(ctx, http.StatusNotFound, "key/id missing")
			return
		}

		if node, hopCount, _, err := r.local.SetValue(id, req.Value); err == nil {
			ctx.JSON(http.StatusOK, &changeResponse{
				NodeUrl:  node.Url(),
				HopCount: hopCount})
		} else {
			log.Errorf(err.Error())
			returnError(ctx, http.StatusInternalServerError, "an error ocurred")
		}
	} else {
		returnError(ctx, http.StatusBadRequest, "bad request")
	}
}

// Delete a value by a key
// @Summary Delete a value by a key
// @Schemes
// @Description Delete a value by a key
// @Tags api/v1
// @Accept json
// @Produce json
// @Param key path string true "Key of the value to delete"
// @Success 200 {object} changeResponse
// @Failure 400 {object} errorResponse
// @Failure 500 {object} errorResponse
// @BasePath /api/v1
// @Router /value-by-key/{key} [delete]
func (r *restServer) delValueByKey(ctx *gin.Context) {
	if keyParam := ctx.Param("key"); len(keyParam) > 0 {
		if node, hopCount, _, err := r.local.DelValue(dht.MakeId(keyParam)); err == nil {
			ctx.JSON(http.StatusOK, &changeResponse{
				NodeUrl:  node.Url(),
				HopCount: hopCount})
		} else {
			log.Errorf(err.Error())
			returnError(ctx, http.StatusInternalServerError, "an error ocurred")
		}
	} else {
		returnError(ctx, http.StatusBadRequest, "key missing")
	}
}

// Delete a value by an id
// @Summary Delete a value by an id
// @Schemes
// @Description Delete a value by an id
// @Tags api/v1
// @Accept json
// @Produce json
// @Param id path string true "Id of the value to delete"
// @Success 200 {object} changeResponse
// @Failure 400 {object} errorResponse
// @Failure 500 {object} errorResponse
// @BasePath /api/v1
// @Router /value-by-id/{id} [delete]
func (r *restServer) delValueById(ctx *gin.Context) {
	if idParam := ctx.Param("id"); len(idParam) > 0 {
		if id, err := dht.MakeIdString(idParam); err == nil {
			if node, hopCount, _, err := r.local.DelValue(id); err == nil {
				ctx.JSON(http.StatusOK, &changeResponse{
					NodeUrl:  node.Url(),
					HopCount: hopCount})
			} else {
				log.Errorf(err.Error())
				returnError(ctx, http.StatusInternalServerError, "an error ocurred")
			}
		} else {
			returnError(ctx, http.StatusBadRequest, "id invalid")
		}
	} else {
		returnError(ctx, http.StatusBadRequest, "id missing")
	}
}

// Get a value by a key
// @Summary Get a value by a key
// @Schemes
// @Description Get a value by a key
// @Tags api/v1
// @Produce json
// @Param key path string true "Key of the value to get"
// @Success 200 {object} getValueResponse
// @Failure 400 {object} errorResponse
// @Failure 404 {object} errorResponse
// @BasePath /api/v1
// @Router /value-by-key/{key} [get] getValueRequest
func (r *restServer) getValueByKey(ctx *gin.Context) {
	log.Debugf("restServer.getValueByKey")
	if keyParam := ctx.Param("key"); len(keyParam) > 0 {
		node, hopCount, value, ok, _, err := r.local.GetValue(dht.MakeId(keyParam))
		if err == nil {
			if ok {
				ctx.JSON(http.StatusOK, &getValueResponse{
					NodeUrl:  node.Url(),
					HopCount: hopCount,
					Value:    value})
			} else {
				returnError(ctx, http.StatusNotFound, "value not found")
			}
		} else {
			log.Errorf(err.Error())
			returnError(ctx, http.StatusInternalServerError, "an error ocurred")
		}
	} else {
		returnError(ctx, http.StatusBadRequest, "key missing")
	}
}

// Get a value by an id
// @Summary Get a value by an id
// @Schemes
// @Description Get a value by an id
// @Tags api/v1
// @Produce json
// @Param id path string true "Id of the value to get"
// @Success 200 {object} getValueResponse
// @Failure 400 {object} errorResponse
// @Failure 404 {object} errorResponse
// @BasePath /api/v1
// @Router /value-by-id/{id} [get] getValueRequest
func (r *restServer) getValueById(ctx *gin.Context) {
	if idParam := ctx.Param("id"); len(idParam) > 0 {
		if id, err := dht.MakeIdString(idParam); err == nil {
			node, hopCount, value, ok, _, err := r.local.GetValue(id)
			if err == nil {
				if ok {
					ctx.JSON(http.StatusOK, &getValueResponse{
						NodeUrl:  node.Url(),
						HopCount: hopCount,
						Value:    value})
				} else {
					returnError(ctx, http.StatusNotFound, "value not found")
				}
			} else {
				log.Errorf(err.Error())
				returnError(ctx, http.StatusInternalServerError, "an error ocurred")
			}
		} else {
			returnError(ctx, http.StatusBadRequest, "id invalid")
		}
	} else {
		returnError(ctx, http.StatusBadRequest, "id missing")
	}
}

// Get the node fingers
// @Summary Get the node fingers
// @Schemes
// @Description Get the node fingers
// @Tags api/v1
// @Produce json
// @Success 200 {object} fingerListResponse
// @Failure 400 {object} errorResponse
// @BasePath /api/v1
// @Router /fingers [get]
func (r *restServer) getFingers(ctx *gin.Context) {
	res := new(fingerListResponse)

	res.NodeId = dht.MakeId(r.local.Url()).ToHex()
	res.NodeUrl = r.local.Url()
	if r.local.predecessor == nil {
		res.PredecessorUrl = ""
		res.PredecessorId = ""
	} else {
		res.PredecessorUrl = r.local.predecessor.Url()
		res.PredecessorId = r.local.predecessor.Id().ToHex()
	}
	res.Fingers = make([]fingerResponse, len(r.local.fingers))

	for i, f := range r.local.fingers {
		res.Fingers[i].FingerIndex = i
		res.Fingers[i].SuccessorUrl = f.successor.Url()
		res.Fingers[i].SuccessorId = f.successor.Id().ToHex()
		res.Fingers[i].IntervalStart = r.local.fingers[i].intervalStart.ToHex()
	}
	ctx.JSON(http.StatusOK, res)
}

func swaggerRedirectingHandler(c *gin.Context) {
	if len(c.Request.RequestURI) <= 9 {
		if c.Request.RequestURI == "/swagger" || c.Request.RequestURI == "/swagger/" {
			c.Redirect(http.StatusFound, "/swagger/index.html")
			return
		}
	}
	swaggerHandler(c)
}

func openapiRedirectingHandler(c *gin.Context) {
	if len(c.Request.RequestURI) <= 9 {
		if c.Request.RequestURI == "/openapi" || c.Request.RequestURI == "/openapi/" {
			c.Redirect(http.StatusFound, "/openapi/index.html")
			return
		}
	}
	swaggerHandler(c)
}

// @Title DHT-Chord-Key-Value-Store
// @Description Distributed Hash Table - Chord based Key-Value-Storage - created by Stefan Woyde for the course `Verteilte Systeme` at `FH-Erfurt`
// @Version 1.0
// @BasePath /api/v1
// @Contact.name Stefan Woyde
// @Contact.url https://moodle.fh-erfurt.de/user/profile.php?id=5422
// @Contact.email stefan.woyde@fh-erfurt.de
func (r *restServer) registerMethods() {
	docs.SwaggerInfo.BasePath = "/api/v1"

	api := r.gin.Group("/api")
	{
		v1 := api.Group("/v1")
		{
			v1.POST("/value", r.setValue)
			v1.DELETE("/value-by-key/:key", r.delValueByKey)
			v1.DELETE("/value-by-id/:id", r.delValueById)
			v1.GET("/value-by-key/:key", r.getValueByKey)
			v1.GET("/value-by-id/:id", r.getValueById)
			v1.GET("/fingers", r.getFingers)
		}
	}
	r.gin.GET("/swagger/*any", swaggerRedirectingHandler)
	r.gin.GET("/openapi/*any", openapiRedirectingHandler)
}

func (r *restServer) init(node *LocalNode) {
	gin.SetMode(strings.ToLower(swo.GetEnvOrDefault("DHT_MODE", "RELEASE")))

	r.local = node
	r.gin = gin.New()
	r.gin.Use(gin.Recovery())
	if gin.Mode() != gin.ReleaseMode {
		r.gin.Use(gin.Logger())
	}
	r.registerMethods()
	r.http = &http.Server{Handler: r.gin}
}
