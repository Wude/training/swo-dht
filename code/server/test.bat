@echo off
@REM Test script for Windows in case no GNU make is available.

setlocal
set "SCRIPT_PATH=%~dp0"
set "SCRIPT_PATH=%SCRIPT_PATH:~0,-1%"
cd /D "%SCRIPT_PATH%"

echo Test started...
go test -timeout 60s ./pkg/... -count=1
go test -timeout 30s ./internal/pkg/dht/... -count=1
call :test chord
call :test koorde
echo Test ended.

endlocal

exit /B %ERRORLEVEL%

@REM -------------------------------------------------------------------------

:test

set "PKG_NAME=%~1"

go test -timeout 30s ./internal/pkg/%PKG_NAME%/... -count=1

exit /B 0
