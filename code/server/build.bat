@echo off
@REM Build script for Windows in case no GNU make is available.

setlocal
set "SCRIPT_PATH=%~dp0"
set "SCRIPT_PATH=%SCRIPT_PATH:~0,-1%"
cd /D "%SCRIPT_PATH%"

call :build chord
call :build koorde

endlocal

exit /B %ERRORLEVEL%

@REM -------------------------------------------------------------------------

:build

set "PKG_NAME=%~1"

set "PROJECT=dht-%PKG_NAME%"
set "PROTO_SRC=internal/pkg/%PKG_NAME%/proto/node.proto"
set "SWAG_SRC_DIR=internal/pkg/%PKG_NAME%"
set "SWAG_TRG_DIR=docs/%PKG_NAME%"
set "SWAG_SRC_FILE=rest.go"

swag init -o %SWAG_TRG_DIR% -g %SWAG_SRC_FILE% -d %SWAG_SRC_DIR%
protoc -I=. --go_out=. --go-grpc_out=. %PROTO_SRC%
go get ./cmd/%PROJECT%
go build -o bin/%PROJECT%.exe ./cmd/%PROJECT%

exit /B 0
