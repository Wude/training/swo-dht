ifndef PKG_NAME
$(error PKG_NAME not defined)
endif

# ----------------------------------------------------------------------------

PROJECT			:= dht-$(PKG_NAME)

# ----------------------------------------------------------------------------

rwildcard		= $(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))

PROTO_SRC		:= internal/pkg/$(PKG_NAME)/proto/node.proto
PROTO_TRG		:= $(wildcard internal/pkg/$(PKG_NAME)/proto/*.pb.go)
GO_SRC			:= $(call rwildcard,.,*.go)
SWAG_SRC_DIR	:= internal/pkg/$(PKG_NAME)
SWAG_SRC_FILE	:= rest.go
SWAG_SRC		:= $(SWAG_SRC_DIR)/$(SWAG_SRC_FILE)
SWAG_TRG_DIR	:= docs/$(PKG_NAME)
SWAG_TRG		:= $(SWAG_TRG_DIR)/docs.go

# ----------------------------------------------------------------------------

ifeq ($(OS),Windows_NT)
	BIN_FILE			:= bin/$(PROJECT).exe
else
	BIN_FILE			:= bin/$(PROJECT)
endif

# ----------------------------------------------------------------------------

# Generate GIN-REST-API swagger doc go source files.
# The GIN-REST-API go source file must have been changed.
$(SWAG_TRG): $(SWAG_SRC)
	swag init -o $(SWAG_TRG_DIR) -g $(SWAG_SRC_FILE) -d $(SWAG_SRC_DIR)

# Generate gRPC go source files.
# Proto source file must have been changed.
$(PROTO_TRG): $(PROTO_SRC)
	protoc -I=. --go_out=. --go-grpc_out=. $(PROTO_SRC)

# Get modules the go project depends on and build the project.
# Go source files must have been changed after the target.
$(BIN_FILE): $(GO_SRC) $(PROTO_TRG) $(SWAG_TRG)
	go get ./cmd/$(PROJECT)
	go build $(BUILD_FLAGS) -o $(BIN_FILE) ./cmd/$(PROJECT)

build: $(BIN_FILE)

clean:
	rm -f $(BIN_FILE)

run: build
	@$(BIN_FILE)

test: build
	go test -timeout 30s ./internal/pkg/$(PKG_NAME)/... -count=1

.DEFAULT_GOAL := build
.PHONY: build clean run test
