package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	easy "github.com/t-tomalak/logrus-easy-formatter"
	"gitlab.com/wude/training/swo-dht/internal/pkg/chord"
	"gitlab.com/wude/training/swo-dht/pkg/swo"
)

func init() {
	log.SetFormatter(&easy.Formatter{
		TimestampFormat: "2006-01-02 15:04:05.999 Z07:00",
		LogFormat:       "\033[30;43m[%lvl%]\033[97;40m %time% - \033[37;40m%msg%\n",
	})
	log.SetOutput(os.Stdout)
	log.SetLevel(log.Level(swo.GetEnvOrDefaultUint[uint32]("DHT_LOG_LEVEL", 3)))
}

func main() {
	swo.TryEnableColorMode()
	defer swo.TryDisableColorMode()

	node := chord.StartLocalNode()
	swo.RunAtShutdown(node.Stop)
	swo.WaitForTermination()
}
