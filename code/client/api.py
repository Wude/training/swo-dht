from enum import Enum
import logging
import json
import http.client


class DhtRestClient:
    class Method(Enum):
        GET = 1
        POST = 2
        DELETE = 3

    def __init__(self, host, port):
        self._host = host
        self._port = port
        self._path = "/api/v1/"
        self._conn = None
        self._headers = {
            "Content-Type": "application/json",
            "Accept": "application/json",
        }

    def __enter__(self):
        self._conn = http.client.HTTPConnection(self._host, self._port, timeout=30)
        return self

    def __exit__(self, type, value, traceback):
        self._conn.close()

    def _call(self, method, path, content=None):
        # print(f"{method}:{self._path + path}")
        self._conn.request(
            method.name,
            self._path + path,
            json.dumps(content) if content else None,
            self._headers,
        )

        res = self._conn.getresponse()
        res_data = res.read().decode()
        # print(res_data)
        res_obj = json.loads(res_data)

        return res_obj

    def set_value_by_key(self, key, value):
        """Set a value by a key."""
        return self._call(Method.POST, "value", {"key": key, "value": value})

    def set_value_by_id(self, hash_id, value):
        """Set a value by an id that is a string encoded in hex or base64."""
        return self._call(Method.POST, "value", {"id": hash_id, "value": value})

    def del_value_by_key(self, key):
        """Delete a value by a key."""
        return self._call(Method.DELETE, f"value-by-key/{key}")

    def del_value_by_id(self, hash_id):
        """Delete a value by an id that is a string encoded in hex or base64."""
        return self._call(Method.DELETE, f"value-by-id/{hash_id}")

    def get_value_by_key(self, key):
        """Get a value by a key."""
        return self._call(Method.GET, f"value-by-key/{key}")

    def get_value_by_id(self, hash_id):
        """Get a value by an id that is a string encoded in hex or base64."""
        return self._call(Method.GET, f"value-by-id/{hash_id}")

    def get_fingers(self):
        """Get the finger table of the directly asked node."""
        return self._call(Method.GET, "fingers")
