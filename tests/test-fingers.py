#!/usr/bin/env python


class Finger:
    def __init__(self, start):
        self.start = start
        self.end = 0


def print_fingers(bits, id_count, id):
    print(f"\tid: {id}")
    fingers = []

    for bit_index in range(bits):
        start = (id + (2**bit_index)) % id_count
        fingers.append(Finger(start))
        if bit_index > 0:
            fingers[bit_index - 1].end = start
    fingers[bits - 1].end = id

    for bit_index, finger in enumerate(fingers):
        print(
            f"\t\tfingers[{bit_index}]: {{ start: {finger.start}, end: {finger.end} }}"
        )


def print_nodes(bits):
    id_count = 2**bits
    print(f"bits: {bits}; id_count: {id_count}")

    for id in range(id_count):
        print_fingers(bits, id_count, id)


if __name__ == "__main__":
    # Print finger table intervals for ids with the given bit amount.
    print_nodes(4)
