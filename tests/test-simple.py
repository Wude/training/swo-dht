import sys

sys.path.insert(1, "../code/client")
from api import DhtRestClient

dht = DhtRestClient("localhost", 8000)

print(dht.set_value_by_key("1", "test"))
print(dht.set_value_by_key("2", "text"))

# print(dht.del_value_by_key('1'))
# print(dht.del_value_by_key('2'))

print(dht.get_value_by_key("1"))
print(dht.get_value_by_key("2"))

print(dht.get_fingers())
