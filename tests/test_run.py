import unittest

import logging
import sys
import bitstring

sys.path.append("../code/client")
from api import DhtRestClient


class Test1(unittest.TestCase):
    __HOST = "localhost"
    __PORT = 8000

    def test_1(self):
        with DhtRestClient(Test1.__HOST, Test1.__PORT) as dht:
            print(dht.set_value_by_key("1", "test"))
            print(dht.set_value_by_key("2", "text"))

            # print(dht.del_value_by_key('1'))
            # print(dht.del_value_by_key('2'))

            print(dht.get_value_by_key("1"))
            print(dht.get_value_by_key("2"))

            fingers = dht.get_fingers()

            # import base64

            # node_id = base64.b64decode(fingers["nodeId"], altchars="-_")
            # print(node_id)
            # print(node_id.hex())
            # bits = bitstring.Bits(hex="0x" + node_id.hex())
            # print(bits.bin)

            # finger = fingers["fingers"][0]
            # interval_start = base64.b64decode(finger["intervalStart"], altchars="-_")
            # print(bitstring.Bits(hex="0x" + interval_start.hex()).bin)

            # self.assertEqual(
            #     "heartbeat_interval_seconds", util.to_snake_case("HeartbeatIntervalSeconds")
            # )
            # self.assertEqual("udp_receiver_port", util.to_snake_case("UDPReceiverPort"))
            # self.assertEqual("udp", util.to_snake_case("UDP"))
            # self.assertEqual("receiver", util.to_snake_case("Receiver"))
            # self.assertEqual("port", util.to_snake_case("Port"))


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
