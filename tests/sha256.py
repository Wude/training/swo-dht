import hashlib
import binascii
import base64


def toSha256(value):
    return hashlib.sha256(value.encode("utf-8")).digest()


def toHex(hashBytes):
    return binascii.hexlify(hashBytes).decode("utf-8")


def toBase64(hashBytes):
    return base64.urlsafe_b64encode(hashBytes).decode("utf-8")


def printHash(value):
    hashBytes = toSha256(value)
    print(f"{value+(' '*(20-len(value)))} -> {toHex(hashBytes)}; {toBase64(hashBytes)}")


if __name__ == "__main__":
    for i in range(10):
        printHash(f"localhost:{8000+i}")
    for i in range(11):
        printHash(f"chord-node-{8000+i}:{8000+i}")
    for i in range(10):
        printHash(f"{i}")
    printHash("👍")
    printHash("123")
