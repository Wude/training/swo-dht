#!/bin/sh

(
SCRIPT_DIR=`dirname "$0"`
SCRIPT_DIR=`realpath "$SCRIPT_DIR"`

cd $SCRIPT_DIR
python3 gen-config.py chord=8000:8009

cd ../code/server
docker build -t swo-dht-chord -f ./cmd/dht-chord/Dockerfile .
)
