@echo off

setlocal
set "SCRIPT_PATH=%~dp0"
set "SCRIPT_PATH=%SCRIPT_PATH:~0,-1%"

cd /d "%SCRIPT_PATH%"
python3 gen-config.py chord=8000:8009

cd ../code/server
docker build -t swo-dht-chord -f ./cmd/dht-chord/Dockerfile .
endlocal
