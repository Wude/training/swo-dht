# Distributed Hash Table

The project was developed with `VSCodium` (libre binaries of `Visual Studio Code`).

Extension recommendations are included.

The project includes VSCode-Tasks for different purposes (build, test, etc.), click `Terminal->Run Task...` in the menu to see.

Once a node is started, the OpenAPI-page as documentation to the REST-API becomes available. For node `localhost:8000` open http://localhost:8000/openapi to see.

## Development

* `Python 3`: docker compose config generation, test API
* `Docker`: easly running multiple nodes
* `Go` at least version 1.20: build

Go Command Line Tools:
```sh
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
go install github.com/swaggo/swag/cmd/swag@latest
```
