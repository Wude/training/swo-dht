\chapter{Umsetzung}
\label{sec:Umsetzung}

Die Umsetzung nutzt die Programmiersprache Go. Eine Code-Übersicht ist in Abbildung~\ref{fig:code_chord} zu sehen.

\begin{figure}[!ht]
	\centering
	\caption{Code-Übersicht in Anlehnung an ein UML-Klassendiagramm}
	\label{fig:code_chord}

	\hspace*{-1.25cm}
	\scalebox{.625}{
		\texttt{\input{../images/code-overview.latex}}
	}
\end{figure}

\clearpage

% ------------------------------------------------------------------------------
\section{Chord-ID}

Zur Verwaltung der Chord-ID wurde ein eigener Datentyp geschaffen (\texttt{dht.Id}, siehe Listing~\ref{lis:id}). Die ID wird auf Basis von SHA-256 erstellt. Die Fingertabellen der Knoten haben damit also letztlich 256 Einträge. Zur Berechnung der Fingertabelleneinträge steht die Methode \texttt{AddPow2} zur Verfügung (siehe Listing~\ref{lis:id_addpow2}). Es sind auch Methoden zur Prüfung auf Intervall-Mitgliedschaft enthalten, Start- und Endbereichsgrenzen können jeweils als \enquote{inklusiv} oder \enquote{exklusiv} benannt werden.

\begin{lstlisting}[
	mathescape=true,
	style=go,
	label={lis:id},
	caption={Interne Darstellung der Chord-ID},
]
const (
	IdBitCount       = 256 // SHA256
	IdBitUpperIndex  = IdBitCount - 1
	IdByteCount      = IdBitCount / 8
	IdByteUpperIndex = IdByteCount - 1
)

type Id struct {
	data [IdByteCount]byte
}
\end{lstlisting}

\begin{lstlisting}[
	mathescape=true,
	style=go,
	label={lis:id_addpow2},
	caption={Ermittlung eines Fingerintervallstartwerts},
]
// Computes a finger id
func (id Id) AddPow2(idBitIndex int) (newId Id) {
	if idBitIndex < 0 {
		panic("id bit below range")
	}
	if idBitIndex > IdBitUpperIndex {
		panic("id bit above range")
	}
	copy(newId.data[:], id.data[:])

	// The results of SHA2 use big endian byte order,
	// therefore the offset has to be reversed.
	offset := idBitIndex / 8
	e := idBitIndex - (offset * 8)
	offset = IdByteUpperIndex - offset
	bOld := newId.data[offset]
	bNew := bOld + (1 << e) // Adding the binary exponentiation result inside the significant byte

	for {
		newId.data[offset] = bNew
		if bOld < bNew {
			// No overflow
			break
		}
		offset--
		if offset < 0 {
			// Overflow is to be ignored
			// Masking the overflow acts as binary modulo
			break
		}
		// Applying overflow to the next byte
		bOld = newId.data[offset]
		bNew = bOld + 1
	}
	return
}
\end{lstlisting}

% ------------------------------------------------------------------------------
\section{Protobuf-Generierung}

Dem Projekt liegt eine \enquote{proto}-Datei bei (siehe Listing~\ref{lis:proto}. Sie beschreibt die Protobuf-Schnittstelle, über die die gRPC-Werkzeuge Code generieren. Der generierte Code enthält neben Schnittstellen für Server und Client auch eine grundlegende Implementation für die generierte Client-Schnittstelle. Die Datentypen und Funktionen müssen \enquote{nur} noch eingebunden werden.

\begin{lstlisting}[
	mathescape=true,
	style=proto,
	label={lis:proto},
	caption={Erweiterte Schnittstellenbeschreibung für Chord},
]
syntax = "proto3";

package chord;

option go_package = "internal/pkg/chord/proto";

message Void {}

message NodeInfo {
  bytes id = 1;
  string url = 2;
}

message NodeAndHopInfo {
  NodeInfo node = 1;
  uint32 hopCount = 2;
}

message Id {
  bytes id = 1;
}

message IdAndValue {
  bytes id = 1;
  string value = 2;
}

message ValueInfo {
  NodeAndHopInfo responsibleNode = 1; // info of the node which is currently responsible for the value
  string value = 2;
  bool found = 3;
}

service Node {
  rpc Successor(Void) returns (NodeInfo);
  rpc Predecessor(Void) returns (NodeInfo);
  rpc FindSuccessor(Id) returns (NodeAndHopInfo);
  rpc FindPredecessor(Id) returns (NodeAndHopInfo);
  rpc ClosestPrecedingFinger(Id) returns (NodeInfo);
  rpc NotificationByPredecessor(NodeInfo) returns (Void);
  rpc NotificationBySuccessor(NodeInfo) returns (Void);
  rpc CheckNode(NodeInfo) returns (Void);
  rpc SetReplicatedValue(IdAndValue) returns (Void);
  rpc SetValue(IdAndValue) returns (NodeAndHopInfo);
  rpc DelValue(Id) returns (NodeAndHopInfo);
  rpc GetValue(Id) returns (ValueInfo);
}
\end{lstlisting}

% ------------------------------------------------------------------------------
\section{\texttt{LocalNode}}

Der lokale Knoten, Datentyp \texttt{LocalNode}, stellt die lokal laufende Instanz eines Chord-Knotens dar. Er regelt die Speicherung der Schüssel-Wert-Paare und den Zugriff darauf (siehe Listing~\ref{lis:localnode_setvalue} als Beispiel). Die Umsetzung des Chord-Protokolls zur Kommunikation mit anderen Knoten ist ebenfalls hier verortet. Der in Abschnitt~\ref{sec:Chord} beschriebene Pseudocode wurde unter Einarbeitung der Rahmenbedingungen in Go-Code übersetzt. Als Beispiel für eine dieser Methoden ist \texttt{stabilize} in Listing~\ref{lis:localnode_stabilize} zu sehen, sie lauft im Hintergrund alle 10 Sekunden.

Im lokalen Knoten werden die \texttt{Listener} des REST-Servers und des gRPC-Servers zusammengeführt. Nur durch den Multiplexer kann der Knoten einen TCP-Port-\texttt{Listener} für alle Bereiche nutzen (siehe Listing~\ref{lis:localnode_start}).

\begin{lstlisting}[
	mathescape=true,
	style=go,
	label={lis:localnode_setvalue},
	caption={Interne Speicherung eines Schlüssel-Wert-Paares},
]
// Store the value for the id on the local node.
// Premise is that the local node is actually responsible.
func (n *LocalNodeBase) SetValue(id Id, value string) (NodeInfo, error) {
	rw := n.krw.Lock(id) // Lock by key
	defer rw.Unlock()

	filename := n.keyValueFilepath(id)
	file, err := os.Create(filename)
	if err != nil {
		return n.NodeInfo, err
	}
	defer file.Close()
	file.WriteString(value)

	return n.NodeInfo, nil
}
\end{lstlisting}

\begin{lstlisting}[
	mathescape=true,
	style=go,
	label={lis:localnode_stabilize},
	caption={Erneute Ermittlung des direkten Nachfolgers dieses Knotens},
]
// Fix the successor.
func (n *LocalNode) stabilize() error {
	node, unavailable, err := n.fingers[0].successor.Predecessor()
	for unavailable {
		n.replaceFinger(0)
		node, unavailable, err = n.fingers[0].successor.Predecessor()
	}

	if err != nil {
		log.Info(err.Error())
	} else if node != nil {
		if n == n.fingers[0].successor ||
			n.Id().Equal(n.fingers[0].successor.Id()) ||
			node.Id().BetweenEE(n.Id(), n.fingers[0].successor.Id()) {

			n.fingers[0].successor = node
		}
	}

	unavailable, err = n.fingers[0].successor.NotificationByPredecessor(n)
	for unavailable {
		n.replaceFinger(0)
		unavailable, err = n.fingers[0].successor.NotificationByPredecessor(n)
	}
	if err != nil {
		log.Info(err.Error())
	}
	return nil
}
\end{lstlisting}

\begin{lstlisting}[
	mathescape=true,
	style=go,
	label={lis:localnode_start},
	caption={Auszug aus der Funktion \texttt{StartLocalNode}},
]
		// ...
		var lis net.Listener
		lis, err = net.Listen("tcp", fmt.Sprintf(":%d", n.Port))
		if err == nil {
			n.cmux = cmux.New(lis)

			grpcLis := n.cmux.MatchWithWriters(cmux.HTTP2MatchHeaderFieldSendSettings("content-type", "application/grpc"))
			httpLis := n.cmux.Match(cmux.Any())

			go func() { _ = n.proto.grpc.Serve(grpcLis) }()
			go func() { _ = n.cmux.Serve() }()

			if err := n.join(n.JoinNodeInfo); err != nil {
				log.Error(err.Error())
			}
			go func() { _ = n.rest.http.Serve(httpLis) }()
			// ...
\end{lstlisting}

\section{\texttt{restServer}}

Der REST-Server, Datentyp \texttt{restServer}, stellt nach außen Methoden zur Verwaltung von Schlüssel-Wert-Paaren und zum Abruf der Fingertabelle zur Verfügung (siehe Listing~\ref{lis:rest}) und nutzt dafür den \texttt{LocalNode}. Darüber hinaus enthält er eine selbst-gehostete OpenAPI (Swagger), über die die REST-API dokumentiert ist.

\begin{lstlisting}[
	mathescape=true,
	style=go,
	label={lis:rest},
	caption={Methoden REST-API},
]
func (r *restServer) registerMethods() {
	docs.SwaggerInfo.BasePath = "/api/v1"

	api := r.gin.Group("/api")
	{
		v1 := api.Group("/v1")
		{
			v1.POST("/value", r.setValue)
			v1.DELETE("/value-by-key/:key", r.delValueByKey)
			v1.DELETE("/value-by-id/:id", r.delValueById)
			v1.GET("/value-by-key/:key", r.getValueByKey)
			v1.GET("/value-by-id/:id", r.getValueById)
			v1.GET("/fingers", r.getFingers)
		}
	}
	r.gin.GET("/swagger/*any", swaggerRedirectingHandler)
	r.gin.GET("/openapi/*any", openapiRedirectingHandler)
}
\end{lstlisting}

\section{\texttt{protoServer}}

Der Datentyp \texttt{protoServer} implementiert eine generierte gRPC-Schnittstelle und stellt Methoden bereit, über die ein entfernt laufender Chord-Knoten auf diesen lokalen Knoten zugreifen kann. Als Beispiel ist die Methode zum Setzen eines Schlüssel-Wert-Paares in Listing~\ref{lis:protoserver_setvalue} zu sehen.

\begin{lstlisting}[
	mathescape=true,
	style=go,
	label={lis:protoserver_setvalue},
	caption={Von einem anderen Knoten kommende Anfrage zur Speicherung eines Schlüssel-Wert-Paares},
]
func (s *protoServer) SetValue(ctx context.Context, req *proto.IdAndValue) (*proto.NodeAndHopInfo, error) {
	id := req.GetId()
	value := req.GetValue()
	node, hopCount, err := s.local.SetValue(dht.MakeIdSlice(id), value)
	return &proto.NodeAndHopInfo{
			Node: &proto.NodeInfo{
				Id:  node.Id().ToByteSlice(),
				Url: node.Url()},
			HopCount: hopCount},
		err
}
\end{lstlisting}

% ------------------------------------------------------------------------------
\section{\texttt{remoteNode}}

Der Remote-Knoten, Datentyp \texttt{remoteNode}, kapselt den Zugriff auf die über gRPC generierte Client-Kommunikationsschnittstelle. Der lokale Knoten speichert zu jeder bekannten Adresse eines außerhalb liegenden Chord-Knotens ein Remote-Knoten-Objekt. Bei Auftauchen der Adresse während der Kommunikation wird das Remote-Knoten-Objekt eingesetzt. Falls ein Remote-Knoten nicht wie erwartet reagiert, wird er bei einer Anfrage als nicht verfügbar gemeldet. Ein Beispiel für einen RPC-Aufruf ist in Listing~\ref{lis:remotenode_setvalue} zu sehen.

\begin{lstlisting}[
	mathescape=true,
	style=go,
	label={lis:remotenode_setvalue},
	caption={An einen anderen Knoten gerichtete Anfrage zur Speicherung eines Schlüssel-Wert-Paares},
]
func remoteCall[TRequest any, TResponse any](
	action func(context.Context, *TRequest, ...grpc.CallOption) (*TResponse, error),
	req *TRequest,
) (
	*TResponse, bool, error,
) {
	ctx, cancel := context.WithTimeout(
		context.Background(), remoteCallTimeout)
	defer cancel()

	var unavailable bool
	result, err := action(ctx, req)
	if err != nil {
		errStatus, ok := status.FromError(err)
		if ok {
			statusCode := errStatus.Code()
			unavailable = statusCode != codes.OK
		}
	}
	return result, unavailable, err
}

func (n *remoteNode) SetValue(id dht.Id, value string) (
	dht.NodeInfo, uint32, bool, error,
) {
	res, unavailable, err := remoteCall(
		n.client.SetValue, &proto.IdAndValue{
			Id:    id.ToByteSlice(),
			Value: value,
		})
	if err != nil {
		return dht.NodeInfo{}, 0, unavailable, err
	}
	return n.NodeInfo, res.HopCount, unavailable, err
}
\end{lstlisting}

\clearpage

% ------------------------------------------------------------------------------
\section{Helfer-Code}

Im Verlauf der Implementation hat sich Helfer-Code angesammelt, der für den eigentlichen Zweck nebenschlich ist, aber dennoch gebraucht wird. Hier sind die wichtigsten davon aufgeführt.

\subsection{\texttt{KeyedRWMutex}}

In der Sprache Go sind Mutexe verfügbar, unter Anderem für die Verwaltung kombinierter Lese-/Schreibzugriffe. Zur Verwaltung der Schlüssel-Wert-Paare unseres einfachen DHTs wird eine Möglichkeit gebraucht, über einen Schlüssel Zugriffe zu verwalten. Diese Lücke füllt der \texttt{KeyedRWMutex}. Dieser Datentyp und seine Methoden bieten eine Verwaltung von Lese-/Schreibzugriffen gruppiert nach einem Schlüssel. Datentyp und Methodensignaturen sind in Listing~\ref{lis:keyedrwmutex} dargestellt.

\begin{lstlisting}[
	mathescape=true,
	style=go,
	label={lis:keyedrwmutex},
	caption={\texttt{KeyedRWMutex} und Methodensignaturen},
]
// A read-write-mutex which groups lockers by keys.
type KeyedRWMutex struct {
	pool    sync.Pool                  // Object pool.
	mu      sync.RWMutex               // Map locker.
	lockers map[any]*KeyedRWMutexEntry // Lockers by keys.
}

// A keyed read-write-mutex.
type KeyedRWMutexEntry struct {
	k     *KeyedRWMutex
	key   any
	count int32 // Synchronization counter
	mu    sync.RWMutex
}

// Initialize the given keyed read-write-mutex.
func (k *KeyedRWMutex) Init()

// Lock a key for reading.
func (k *KeyedRWMutex) RLock(key any) *KeyedRWMutexEntry

// Unlock a keyed [Locker] which was locked for reading.
func (rw *KeyedRWMutexEntry) RUnlock()

// Lock a key for writing.
func (k *KeyedRWMutex) Lock(key any) *KeyedRWMutexEntry

// Unlock a keyed [Locker] which was locked for writing.
func (rw *KeyedRWMutexEntry) Unlock()
\end{lstlisting}

\subsection{Datei \enquote{\texttt{task.go}}}

Ein Knoten muss für die Dauer seiner Verfügbarkeit verschiedene
 Wartungsaufgaben erfüllen. Diese müssen regelmäßig ausgeführt werden. Auch das Herunterfahrens eines Knotens soll möglichst geordnet (\enquote{graceful}) geschehen. Zur Verwaltung solcher Tasks bietet diese Quelldatei vereinfachende Funktionen an (siehe Listing~\ref{lis:tasks}).

\begin{lstlisting}[
	mathescape=true,
	style=go,
	label={lis:tasks},
	caption={Funktionsignaturen der vereinfachten Taskverwaltung},
]
type Action func() error

// Start a goroutine that executes the given action periodically.
func RunPeriodically(interval time.Duration, action Action) (ctx context.Context, cancel context.CancelFunc)

// Run the given action at shutdown.
func RunAtShutdown(action Action)

// Wait for program termination signal.
func WaitForTermination()
\end{lstlisting}
