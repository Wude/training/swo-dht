\chapter{Grundlagen}
\label{sec:Grundlagen}

% ------------------------------------------------------------------------------
\section{Chord}
\label{sec:Chord}

Chord ist ein Protokoll und Algorithmus für einen verteilten Suchdienst (siehe \cite{smkkb2001chord}). Es beschreibt also Zuständigkeit und Auffindbarkeit eines beteiligten Knotens in Bezug auf Schlüssel. Je nach Anwendung kann ein Schlüssel für Daten oder andere Dienste stehen.

\begin{figure}[!ht]
	\centering
	\caption{Nachfolgeknoten der Schlüssel 6 und 12}
	\label{fig:chord_successors}

	% \hspace*{-1.25cm}
	% \scalebox{1.735}{
		\texttt{\input{../images/chord-ring-successor.latex}}
	% }
\end{figure}

Im Zentrum von Chord steht eine konsistente Hashfunktion, im ursprünglichen Artikel von 2001 wurde dafür SHA-1 ausgewählt. Jedem Schlüssel und auch jedem beteiligten Knoten wird ein unveränderlicher Hashwert zugeordnet. Die Hashwerte bilden einen geschlossenen Kreis, in dem auf den höchsten Wert erneut der niedrigste Wert folgt. Für einen Schlüssel ist derjenige Knoten zuständig, dessen Hash auf diesem Chord-Ring als nächster Nachfolger liegt. Ein Beispiel für die Ermittlung des Nachfolgers wird in Abbildung~\ref{fig:chord_successors} gezeigt, wobei drei Knoten (1, 7, und 11) vorhanden sind. Der Chord-Ring in diesem Beispiel hat die Größe $2^4 = 16$. Jeder Schlüssel-Hash, im Folgenden auch $id$ genannt, hat also $4$ Bit.

\begin{figure}[!ht]
	\centering
	\caption{Fingerbereiche des Chord-Knotens mit der ID \enquote{1}}
	\label{fig:chord_node_1_finger_intervals}

	% \hspace*{-1.25cm}
	% \scalebox{1.735}{
		\texttt{\input{../images/chord-ring-fingers.latex}}
	% }
\end{figure}

Um später die Auffindbarkeit von Knoten, also Nachfolgern eines Schlüssel-Hashs zu verbessern, besitzt jeder Knoten eine sogenannte \enquote{Fingertabelle}. Die Fingertabelle hat ebensoviele Einträge, wie der Schüssel-Hash Bits hat. Für das hier genutzte Beispiel sind das $4$ Einträge. Jeder Eintrag beschreibt die Weiterleitung für einen bestimmten Zuständigkeitsbereich auf dem Chord-Ring. Der Intervalstart eines Fingertabelleneintrags berechnet sich aus $(n + 2^i) \bmod 2^m$, wobei $n$ der Hashwert des Knotens ist, $i$ der Index des Fingers und $m$ die Anzahl der Hash-Bits. Abbildung~\ref{fig:chord_node_1_finger_intervals} zeigt die Fingerintervalle für den Knoten \enquote{1}. Wie die Fingertabelle zum Einsatz kommt, zeigt Listing~\ref{lis:chord_search}. Der erste Eintrag der Fingertabelle verweist dabei auf den direkten Nachfolgenoten, deshalb die vereinfachende Definition zu Beginn.

\begin{lstlisting}[
	mathescape=true,
	style=pseudocode,
	label={lis:chord_search},
	caption={Pseudocode zum Finden eines Nachfolgeknotens zu einem Schlüssel-Hash},
]
	#define successor fingers[0].successor

	// ask node $n$ to find $id$'s successor
	$n$.find_successor($id$)
		$n'$ = find_predecessor($id$);
		return $n'$.successor;

	// ask node $n$ to find $id$'s predecessor
	$n$.find_predecessor($id$)
		$n'$ = $n$
		while ($id$ $\notin$ ($n'$, $n'$.successor])
			$n'$ = $n'$.closest_preceding_finger($id$);
		return $n'$;

	// return closest finger preceding $id$
	$n$.closest_preceding_finger($id$)
		for $i$ = $m - 1$ downto $0$
			if (fingers[$i$].successor $\in$ ($n$, $id$))
				return fingers[$i$].successor;
		return $n$;
\end{lstlisting}

Im Verlauf des Betriebs werden die Fingertabelleneinträge veralten, die Routinginformation werden also unzureichend. Um dem entgegenzuwirken, müssen die Einträge in regelmäßigen Zeitabständen gewartet werden. Zur Wartung wird der direkte Vorgängerknoten in einer eigenen Variable vermerkt. Damit hat der Knoten auch eine Verbindung zu seinen Vorgängern und nicht nur zu seinen Nachfolgern. Listing~\ref{lis:chord_stabilization} zeigt eine einfache Möglichkeit zur Wartung der Fingertabelle.

\begin{lstlisting}[
	mathescape=true,
	style=pseudocode,
	label={lis:chord_stabilization},
	caption={Pseudocode zur Wartung der Fingertabelle},
]
	$n$.join($n'$)
		predecessor = nil;
		successor = $n'$.find_successor($n$)

	// perodically verify $n$'s immedate successor,
	// and tell the successor about it.
	$n$.stabilize()
		$n'$ = successor.predecessor;
		if ($n'$ $\in$ ($n$, successor))
			successor = $n'$;
		successor.notify($n$);

	// $n'$ thinks it might be our predecessor.
	$n$.notify($n'$)
		if (predecessor is nil or $n'$ $\in$ (predecessor, $n$))
			predecessor = $n'$;

	// perodically refresh finger table entries.
	$n$.fix_fingers()
		$i$ = random index into fingers;
		fingers[$i$].successor = $n$.find_successor(finger[$i$].start);
\end{lstlisting}

Der im ursprünglichen Artikel beschriebene Algorithmus ist leider nicht fehlerfrei (siehe \cite{zave2012modelingchord}), weshalb in einer zur produktiven Nutzung vorgesehenen Implementation eigene Optimierungen vorgenommen werden sollten.

% ------------------------------------------------------------------------------
\section{Die Programmiersprache \enquote{Go}}

\enquote{Go} ist statisch typisiert und lehnt sich in seiner Syntax an \enquote{C} an (siehe \cite{website:golang}). Darüberhinaus bietet die Sprache eine \enquote{Garbage Collection} (automatische Speicherbereinigung) und \enquote{Concurrency} über \enquote{Channels} an. Statt der in vielen andere Programmiersprache üblichen Vererbung, wird Objektorientierung in \enquote{Go} über Interfaces und Komposition erreicht.

% ------------------------------------------------------------------------------
\section{Protocol Buffers}

\enquote{Protocol Buffers}, abgekürzt \enquote{Protobuf}, ist ein Datenformat zur Serialisierung (siehe \cite{website:protobuf}). Die zu serialisierenden Datenformate werden über eine zugehörige Schnittstellenbeschreibungssprache (IDL, \enquote{Interface Description Language}) beschrieben. Die eigentliche Serialisierung erfolgt in einem binären Datenformat.

% ------------------------------------------------------------------------------
\section{gRPC}

\enquote{gRPC} (\enquote{gRPC Remote Procedure Calls}) ist ein RPC-Framework (\enquote{Remote Procedure Call}). Das heißt es ist ein Framework zur Bereitstellung einer Möglichkeit, auf entfernten Systemen Funktionsaufrufe zu starten. Es nutzt HTTP/2 für den Transport und \enquote{Protocol Buffers} für Datenformate (\cite{website:grpc}).

% ------------------------------------------------------------------------------
\section{OpenAPI}

Die \enquote{OpenAPI} Spezifikation beschreibt eine Möglichkeit, die Struktur einer HTTP-API zu beschreiben (siehe \cite{website:openapi}). Dieses Beschreibungsformat kann dazu genutzt werden, eine sich selbst beschreibende HTTP-API zu erstellen. Die Dokumentation kann parallel zur eigentlichen API in Form einer Webseite angeboten werden. Diese Spezifikation wurde auf Basis der Swagger Spezifikation (SmartBear Software) verfasst.
